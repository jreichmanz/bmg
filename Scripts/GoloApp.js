﻿(function() {
    'use strict';

    angular.module('GoloApp', [
            // Angular modules 
            "angularValidator",
            "ngFileUpload",
            // Custom modules 

            // 3rd Party Modules
            "ui.router"
        ]).config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state("healthanalysis", {
	        url: "/health-analysis",
	
	        views: {
	
	            "header": {templateUrl: "view/layout/pageHeader.html"},
	            "content": { templateUrl: "view/mha-questionnaire.html" }
	        }
	
	    })
	.state("fueltracking", {
	            url: "/fuel-tracking",
	
	            views: {
	
	                "header": {templateUrl: "view/layout/pageHeader.html"},
	                "content": { templateUrl: "view/fuel_tracking.html" }
	            }
	
	
	})
        .state("progresstracking", {
            url: "/progress-tracking",

            views: {
              "header": {templateUrl: "view/layout/pageHeader.html"},
                "progtrack": { templateUrl: "view/progress_tracking.html" }

            }

        })
        .state("mhatracking", {
            url: "/mha-tracking",

            views: {
               "header": {templateUrl: "view/layout/pageHeader.html"},
                "content": { templateUrl: "view/mha-tracking.html" }

            }

        })

        .state("resultsSummary", {
            url: "/results-summary",

            views: {
              "header": {templateUrl: "view/layout/pageHeader.html"},
                "content": { templateUrl: "view/data_summary.html" }
            }

        })
        .state("mhaResults", {
            url: "/mha-results",

            views: {
                "fullpage": { templateUrl: "view/mha-results.html" }
            }

        })
        .state("default", {
            url: "/",

            views: {
            	 "header": {templateUrl: "view/layout/pageHeader.html"},
                 "content": { templateUrl: "view/content/golo-rescue-plan/my-tracking.html" }
            }

        });
            $urlRouterProvider.otherwise('/');
        })
        .run(
            function($injector, $rootScope, $location) {
                var _t = {};
                _t.UsernameRecieved = false;

                _t.CheckIfMHATaken = function() {

                }

                try {

                    var _api = $injector.get("GoloService");

                    $rootScope.$on(_api.GetCurrentUserNameHdlr, function(s, rtn) {
                        if (rtn != "") {
                            _api.GetMetabolicRateForCurrentUser();
                        }
                    });

                    $rootScope.$on(_api.getMetabolicRateForCurrentUserHdlr, function(s, rtn) {
                        if (rtn != "") {
                            // user has a metabolic rate
                            $rootScope.PDFAvailable = true;
                            
                        } else {
                            // they do not have one.. send default page to test
                            //$location.path("/health-analysis");
                           
                        }
                    });


                    _api.GetCurrentUserName();


                } catch (e) {

                }


            }
        );

})();
