﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('DataSummaryCtrl', DataSummaryCtrl);

    DataSummaryCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function DataSummaryCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'DataSummaryCtrl';
        vm.Api = GoloService;

        // Variables
        vm.FormData = {};
        vm.Creds = {};
        vm.LoggedIn = false;

        // Functions
        vm.Search = _search;
        vm.UpdateDate = _updateDate;
        vm.Login = _login;

        activate();

        function activate() {
            $('input.datepicker').Zebra_DatePicker({
                format: "m/d/Y",
                onSelect: vm.UpdateDate
            });
        }

        function _search() {
            if (vm.FormData.FromDate && vm.FormData.ToDate) {
                vm.Api.GenerateOverallMHADataSummary(vm.FormData.FromDate, vm.FormData.ToDate);
            } else {
                alert("Please enter both a 'from' and 'to' date.");
            }
        }
        function _updateDate(formattedDate, defaultDate, dateObject, element) {
            if (element.attr("name") == "FromDate") {
                vm.FormData.FromDate = formattedDate;
            } else {
                vm.FormData.ToDate = formattedDate;
            }
        }

        function _login() {
            if (vm.Creds.UserName === "MyGoloAdmin" && vm.Creds.Password === "x%&7aHpk") {
                vm.LoggedIn = true;
            }
        }

        $scope.$on(vm.Api.generateOverallMHADataSummaryHdlr, function (s, rtn) {
            vm.Data = rtn;
        });
    }
})();