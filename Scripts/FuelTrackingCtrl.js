﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('FuelTrackingCtrl', FuelTrackingCtrl);

    FuelTrackingCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function FuelTrackingCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'FuelTrackingCtrl';
        vm.Api = GoloService;

        // Variables
        vm.FormData = {};
        vm.Days = [];
        vm.Totals = {};
        vm.Averages = {};
        vm.CompletedDays = 0;
        vm.Week = 0;
        vm.Year = 0;
        vm.StatusMessage = '';
        vm.CurrentDate = {};
        vm.WeekRange = '';
        vm.FuelTrackWeek = {};
        vm.FuelTrackWeekHistory = [];

        // Functions
        vm.Save = _save;
        vm.SaveFuelTrackWeek = _saveFuelTrackWeek;
        vm.CalculateTotals = _calculateTotals;
        vm.CalculateAverages = _calculateAverages;
        vm.SwitchWeek = _switchWeek;
        vm.InitPopovers = _initPopovers;
        vm.Activate = _activate;
        

        function _activate() {
            vm.Days = generateBlankWeek();

            getWeek();

            vm.Api.GetFuelTrackWeekByWeekAndYear(vm.Week, vm.Year);
            //vm.Api.ListPreviousFiveMetabolicRates();
            vm.InitPopovers();
        }

        function _save() {
            var isValidData = true;

            vm.Days.forEach(function (day) {
                if (day.ActualFitPoints === '') {
                    day.ActualFitPoints = null;
                }
                if (day.ActualFuelConsumed === '') {
                    day.ActualFuelConsumed = null;
                }

                if ((day.ActualFitPoints !== null && day.ActualFuelConsumed === null) || (day.ActualFitPoints === null && day.ActualFuelConsumed !== null)) {
                    isValidData = false;
                }
                day.BaseFuel = vm.FuelTrackWeek.MetabolicRate;
            });

            var date = {
                Week: vm.Week,
                Month: vm.CurrentDate.isoWeekday(1).month() + 1,
                Year: vm.CurrentDate.isoWeekday(1).year()
            }

            if (isValidData) {
                vm.Api.UpdateWeekFuelTracking(date, vm.Days);
            } else {
                console.log("Save", "The data you entered is not valid");
            }
        }

        function _saveFuelTrackWeek() {
            if (!vm.FuelTrackWeek.MetabolicRate || !vm.FuelTrackWeek.FitPointsGoal) {
                alert("Please enter valid values");
                return;
            }

            if (vm.FuelTrackWeek.ID > 0) {
                vm.Api.UpdateFuelTrackWeek(vm.FuelTrackWeek);
            } else {
                vm.FuelTrackWeek.Week = vm.Week;
                vm.FuelTrackWeek.Year = vm.Year;
                vm.Api.AddFuelTrackWeek(vm.FuelTrackWeek);
            }
        }

        function _calculateTotals() {
            vm.CompletedDays = 0;

            vm.Totals = {
                BaseFuelAllowance: 0,
                ActualFitPoints: 0,
                DailyFuelAllowance: 0,
                ActualFuelConsumed: 0,
                MyCompliance: 0
            }

            vm.Days.forEach(function (day) {
                if (day.ActualFitPoints === '') {
                    day.ActualFitPoints = null;
                }
                if (day.ActualFuelConsumed === '') {
                    day.ActualFuelConsumed = null;
                }

                if (day.ActualFitPoints !== null && day.ActualFuelConsumed !== null) {
                    vm.CompletedDays++;
                }

                if (vm.FuelTrackWeek.MetabolicRate !== '' && day.ActualFitPoints !== null) {
                    day.DailyFuelAllowance = parseInt(vm.FuelTrackWeek.MetabolicRate + day.ActualFitPoints);
                    if (day.ActualFuelConsumed >= 1) {
                        day.MyCompliance = parseInt(day.DailyFuelAllowance / day.ActualFuelConsumed * 100);
                    } else {
                        day.MyCompliance = '';
                    }
                } else {
                    day.DailyFuelAllowance = '';
                }

                // Totals
                if (vm.FuelTrackWeek.MetabolicRate > 0) {
                    vm.Totals.BaseFuelAllowance += vm.FuelTrackWeek.MetabolicRate;
                }
                if (day.ActualFitPoints >= 0) {
                    vm.Totals.ActualFitPoints += day.ActualFitPoints;
                }
                if (day.DailyFuelAllowance > 0) {
                    vm.Totals.DailyFuelAllowance += day.DailyFuelAllowance;
                }
                if (day.ActualFuelConsumed >= 0) {
                    vm.Totals.ActualFuelConsumed += day.ActualFuelConsumed;
                }
                if (day.MyCompliance > 0) {
                    vm.Totals.MyCompliance += day.MyCompliance;
                }
            });

            vm.CalculateAverages();
        }

        function _calculateAverages() {
            vm.Averages.BaseFuelAllowance = vm.FuelTrackWeek.MetabolicRate;

            if (vm.CompletedDays > 0) {
                vm.Averages.DailyFuelAllowance = parseInt(vm.Totals.DailyFuelAllowance / vm.CompletedDays);
                vm.Averages.ActualFuelConsumed = parseInt(vm.Totals.ActualFuelConsumed / vm.CompletedDays);
                vm.Averages.MyCompliance = vm.Totals.MyCompliance / vm.CompletedDays;
                vm.Averages.ActualFitPoints = parseInt(vm.Totals.ActualFitPoints / vm.CompletedDays);
            } else {
                vm.Averages.DailyFuelAllowance = 0;
                vm.Averages.ActualFuelConsumed = 0;
                vm.Averages.MyCompliance = 0;
                vm.Averages.ActualFitPoints = 0;
            }
        }

        function _switchWeek(direction) {
            getWeek(direction);

            vm.Api.GetFuelTrackWeekByWeekAndYear(vm.Week, vm.Year);
        }

        function generateBlankWeek() {
            return ([
                { Name: 'Mon', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
                { Name: 'Tues', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
                { Name: 'Wed', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
                { Name: 'Thurs', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
                { Name: 'Fri', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
                { Name: 'Sat', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
                { Name: 'Sun', ActualFitPoints: '', DailyFuelAllowance: '', ActualFuelConsumed: '', MyCompliance: '' },
            ]);
        }

        function getWeek(direction) {
            if (vm.CurrentDate == {}) {
                vm.CurrentDate = moment();
            }

            switch (direction) {
                case 'previous':
                    vm.CurrentDate.subtract(1, 'weeks');
                    break;
                case 'next':
                    vm.CurrentDate.add(1, 'weeks');
                    break;
                default:
                    vm.CurrentDate = moment();
            }

            vm.Week = vm.CurrentDate.isoWeek();
            vm.Year = vm.CurrentDate.isoWeekday(1).year();
            vm.WeekRange = vm.CurrentDate.isoWeekday(1).format('MMM D') + ' - ' + vm.CurrentDate.isoWeekday(7).format('MMM D, YYYY');
        }

        function _initPopovers() {
            var _template = '<div class="popover popover-plus"><div class="arrow"></div><h3 class="popover-title"> <span class="close-popover">X</span></h3><div class="popover-content"></div></div>';
            $('#popoverPMR').popover({
                content: '<p>This number is unique to you, and comes from your Metabolic Health Analysis, it is your Daily Fuel Goal before any exercise.</p>',
                html: true,
                placement: 'bottom',
                template: _template,
                title: 'Personal Metabolic Rate',
                trigger: 'focus'
            });
            $('#popoverFitPoints').popover({
                content: '<p>If you excercise you earn Fit Points. Enter the number of Fit Points you have earned today.</p>',
                html: true,
                placement: 'bottom',
                template: _template,
                title: 'Fit Points',
                trigger: 'focus'
            });
            $('#popoverFuelGoalTotal').popover({
                content: '<p>Your Personal Metabolic Rate PLUS Fit Points earned EQUALS your Daily Fuel Goal.</p>',
                html: true,
                placement: 'bottom',
                template: _template,
                title: 'Fuel Goal Total',
                trigger: 'focus'
            });
            $('#popoverFuelConsumedToday').popover({
                content: '<p>The is the total of all the Fuel Values for the day. So add up breakfast, lunch and dinner, plus any snacks, and enter the total here.</p>',
                html: true,
                placement: 'bottom',
                template: _template,
                title: 'Fuel Consumed Today',
                trigger: 'focus'
            });
            $('#popoverMyCompliance').popover({
                content: '<ul><li>100% means you have matched your Daily Fuel Goal to the fuel you consumed.</li><li> A percentage below 100, such as 93%, indicates you are consuming more fuel than your body needs.</li><li>A percentage above 100, such as 108%, indicates you are consuming less fuel than your body needs.</li></ul><p>Remember, try to stay within +30/-30 fuel values of your Daily Fuel Goal.</p>',
                html: true,
                placement: 'bottom',
                template: _template,
                title: 'My Compliance',
                trigger: 'focus'
            });
            $('#popoverPMRHistory').popover({
                content: 'Previous five PMRs.',
                html: true,
                placement: 'right',
                template: _template,
                title: 'PMR History',
                trigger: 'focus'
            });
        }

        $scope.$on(vm.Api.getFuelTrackingByWeekAndYearHdlr, function (s, rtn) {
            if (rtn) {
                vm.Week = rtn.Week;

                vm.Days = generateBlankWeek();

                if (rtn.Days.length > 0) {
                    rtn.Days.forEach(function (day) {
                        var index = parseInt(day.Day);
                        vm.Days[index].ActualFitPoints = day.ActualFitPoints;
                        vm.Days[index].ActualFuelConsumed = day.ActualFuelConsumed;
                    });
                }
            } else {
                vm.Days = generateBlankWeek();
            }

            vm.CalculateTotals();
        });

        $scope.$on(vm.Api.updateWeekFuelTrackingHdlr, function (s, rtn) {
            if (rtn) {
                // Success
            } else {
                // Fail
            }
        });

        $scope.$on(vm.Api.addFuelTrackWeekHdlr, function (s, rtn) {
            if (rtn) {
                vm.FuelTrackWeek = rtn;

                vm.CalculateTotals();
            }
        });

        $scope.$on(vm.Api.updateFuelTrackWeekHdlr, function (s, rtn) {
            if (rtn) {
                vm.FuelTrackWeek = rtn;

                vm.CalculateTotals();
            }
        });

        $scope.$on(vm.Api.getFuelTrackWeekByWeekAndYearHdlr, function (s, rtn) {
            if (rtn) {
                vm.FuelTrackWeek = rtn;
            } else {
                vm.FuelTrackWeek = {};
            }

            vm.Api.GetFuelTrackingByWeekAndYear(vm.Week, vm.Year);
        });

        $scope.$on(vm.Api.listPreviousFiveMetabolicRatesHdlr, function (s, rtn) {
            var contentHtml = '<table><tr><th style="padding-right: 20px">Date</th><th>PMR</th></tr>';
            rtn.forEach(function (week) {
                contentHtml += '<tr><td style="padding-right: 20px">' + week.Date + '</td><td>' + week.PMR + '</td></tr>';
            });
            contentHtml += '</table>';

            $('#popoverPMRHistory').data('bs.popover').options.content = contentHtml;
        });

        vm.Activate();
    }
})();
