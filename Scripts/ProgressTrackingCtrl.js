﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('ProgressTrackingCtrl', ProgressTrackingCtrl);

    ProgressTrackingCtrl.$inject = ['$scope', '$rootScope', '$location', '$filter', 'GoloService'];

    function ProgressTrackingCtrl($scope, $rootScope, $location, $filter, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'ProgressTrackingCtrl';
        vm.Api = GoloService;

        // Variables
        vm.FormDay = {};
        vm.Days = [];
        vm.Summary = generateBlankSummary();
        vm.ShowSummary = true;

        // Functions
        vm.RegisterDatePickers = _registerDatePickers;
        vm.SaveDay = _saveDay;
        vm.AddDay = _addDay;
        vm.EditDay = _editDay;
        vm.DeleteDay = _deleteDay;
        vm.CalculateSummary = _calculateSummary;
        vm.CalculateDays = _calculateDays;
        vm.UpdateDate = _updateDate;

        activate();

        //$(function () {
        //    $('.date-entry').datetimepicker({
        //        //timepicker: false,
        //        format: 'm/d/Y'
        //    })
        //    .on("input change", function (e) {
        //        var x = 1;
        //        //vm.FormDay.Date = e.target.value;
        //    });
        //});

        function activate() {
            $('input.datepicker').Zebra_DatePicker({
                format: "m/d/Y",
                onSelect: vm.UpdateDate
            });
            vm.Api.ListProgressTrackByUser();
        }

        function _registerDatePickers() {
            //$('.date-entry').datetimepicker({
            //    //timepicker: false,
            //    format: 'm/d/Y'
            //})
            //.on("input change", function (e) {
            //    var x = angular.element(e.target);
            //    angular.element(e.target).triggerHandler('input');
            //    //vm.FormDay.Date = e.target.value;
            //});
        }

        function _saveDay(day) {
            
            if (day.Date !== undefined
                    && day.Weight >= 1 && day.Weight <= 9999
                    && day.Waist >= 1 && day.Waist <= 999
                    && day.Sleep >= 1 && day.Sleep <= 10
                    && day.Energy >= 1 && day.Energy <= 10
                    && day.Mood >= 1 && day.Mood <= 10
                    && day.Stress >= 1 && day.Stress <= 10) {
                day.IsEditing = null;
                day.Week = 0;
                vm.Api.UpdateProgressTrack(day);
            } else {
                alert("Please enter valid values");
            }
        }
        function _updateDate(formattedDate, defaultDate, dateObject, element){
            if (element.attr("name") == "Date") {
                vm.FormDay.Date = formattedDate;
            } else {
                var dayString = element.attr("name").replace("day_", "");
                vm.Days[parseInt(dayString)].Date = formattedDate;
            }
        }

        function _addDay() {
           
            if (vm.FormDay.Date !== undefined
                    && vm.FormDay.Weight >= 1 && vm.FormDay.Weight <= 9999
                    && vm.FormDay.Waist >= 1 && vm.FormDay.Waist <= 999
                    && vm.FormDay.Sleep >= 1 && vm.FormDay.Sleep <= 10
                    && vm.FormDay.Energy >= 1 && vm.FormDay.Energy <= 10
                    && vm.FormDay.Mood >= 1 && vm.FormDay.Mood <= 10
                    && vm.FormDay.Stress >= 1 && vm.FormDay.Stress <= 10) {
                    vm.FormDay.Week = 0;
                    vm.Api.AddProgressTrack(vm.FormDay);

            } else {
                alert("Please enter valid values");
            }
        }

        function _editDay(day, index) {
            day.IsEditing = true;
            setTimeout(function () {
                $('#day_' + index.toString()).Zebra_DatePicker({
                    format: "m/d/Y",
                    onSelect: vm.UpdateDate
                });
            }, 0);
            
            
        }

        function _deleteDay(day) {
            var confirmDelete = confirm("Are you sure you want to delete this?");

            if (confirmDelete) {
                day.Week = 0;
                vm.Api.DeleteProgressTrack(day);
            }
        }

        function _calculateSummary() {
            vm.Summary = generateBlankSummary();

            var totalWeight = 0;
            var totalWaist = 0;
            var sleepStart, sleepEnd;
            var energyStart, energyEnd;
            var moodStart, moodEnd;
            var stressStart, stressEnd;

            vm.Days.forEach(function (day, index) {
                if (day.WeightLost) {
                    totalWeight += day.WeightLost;
                }
                if (day.WaistLost) {
                    totalWaist += day.WaistLost;
                }

                if (index == 0) {
                    sleepStart = day.Sleep;
                    energyStart = day.Energy;
                    moodStart = day.Mood;
                    stressStart = day.Stress;
                }

                if (index == (vm.Days.length - 1)) {
                    sleepEnd = day.Sleep;
                    energyEnd = day.Energy;
                    moodEnd = day.Mood;
                    stressEnd = day.Stress;
                }
            });

            vm.Summary.Total.WeightLost = decimalAdjust('round', totalWeight, -2);
            vm.Summary.Total.WaistLost = decimalAdjust('round', totalWaist, -2);
            vm.Summary.Total.Sleep = sleepEnd;
            vm.Summary.Total.Energy = energyEnd;
            vm.Summary.Total.Mood = moodEnd;
            vm.Summary.Total.Stress = stressEnd;

            vm.Summary.Percentage.WeightLost = decimalAdjust('round', (vm.Days[vm.Days.length - 1].Weight / vm.Days[0].Weight - 1) * 100, -2);
            vm.Summary.Percentage.Sleep = decimalAdjust('round', (sleepEnd / sleepStart - 1) * 100, -2);
            vm.Summary.Percentage.Energy = decimalAdjust('round', (energyEnd / energyStart - 1) * 100, -2);
            vm.Summary.Percentage.Mood = decimalAdjust('round', (moodEnd / moodStart - 1) * 100, -2);
            vm.Summary.Percentage.Stress = decimalAdjust('round', (stressEnd / stressStart - 1) * 100, -2);

            var totalDays = vm.Days[vm.Days.length - 1].TotalDays;
            vm.Summary.Average.WeightLost = decimalAdjust('round', vm.Summary.Total.WeightLost / (totalDays / 7), -2);
            vm.Summary.Average.WaistLost = decimalAdjust('round', vm.Summary.Total.WaistLost / (totalDays / 7), -2);
        }

        function generateBlankSummary() {
            return ({
                Total: {},
                Percentage: {},
                Average: {}
            });
        }

        function _calculateDays(day) {
            var totalDays = 0;
            vm.Days.forEach(function(day, index) {
                if (index >= 1) {
                    var date1 = new Date(day.Date);
                    var date2 = new Date(vm.Days[index-1].Date);
                    totalDays += Math.floor((date1 - date2) / 86400000);

                    day.TotalDays = totalDays;
                    day.WeightLost = decimalAdjust('round', day.Weight - vm.Days[index-1].Weight, -2);
                    day.WaistLost = decimalAdjust('round', day.Waist - vm.Days[index - 1].Waist, -2);
                }
            });
        }

        function sortByDate(a, b) {
            var date1 = new Date(a.Date);
            var date2 = new Date(b.Date);

            if (date1 > date2) {
                return 1;
            }
            if (date1 < date2) {
                return -1;
            }
            return 0;
        }

        function decimalAdjust(type, value, exp) {
            // If the exp is undefined or zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // If the value is not a number or the exp is not an integer...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Shift
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        $scope.$on(vm.Api.listProgressTrackByUserHdlr, function (s, rtn) {
            vm.Days = rtn;
            vm.Days.sort(sortByDate);

            vm.Days.forEach(function (day) {
                day.Date = $filter('date')(day.Date, 'shortDate');
            });

            vm.CalculateDays();

            if (vm.Days.length > 1) {
                vm.CalculateSummary();
                vm.ShowSummary = true;
            } else {
                vm.ShowSummary = false;
            }
        });

        $scope.$on(vm.Api.addProgressTrackHdlr, function (s, rtn) {
            rtn.Date = $filter('date')(rtn.Date, 'shortDate');

            vm.Days.push(rtn);
            vm.Days.sort(sortByDate);

            vm.CalculateDays();

            if (vm.Days.length > 1) {
                vm.CalculateSummary();
                vm.ShowSummary = true;
               
            } else {
                vm.ShowSummary = false;
            }

            vm.FormDay = {};
        });

        $scope.$on(vm.Api.updateProgressTrackHdlr, function (s, rtn) {
            rtn.Date = $filter('date')(rtn.Date, 'shortDate');

            for (var i = 0; i < vm.Days.length; i++) {
                if (rtn.Id == vm.Days[i].Id) {
                    vm.Days[i] = rtn;
                    break;
                }
            }

            vm.CalculateDays();

            if (vm.Days.length > 1) {
                vm.CalculateSummary();
                vm.ShowSummary = true;
            } else {
                vm.ShowSummary = false;
            }
        });

        $scope.$on(vm.Api.deleteProgressTrackHdlr, function (s, rtn) {
            vm.Days = vm.Days.filter(function (day) {
                return day.Id != rtn.Id;
            });

            vm.CalculateDays();

            if (vm.Days.length > 1) {
                vm.CalculateSummary();
                vm.ShowSummary = true;
            } else {
                vm.ShowSummary = false;
            }
        });
    }
})();
