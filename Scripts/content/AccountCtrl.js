﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('AccountCtrl', AccountCtrl);

    AccountCtrl.$inject = ['$scope', '$rootScope', 'GoloService', 'Upload'];

    function AccountCtrl($scope, $rootScope, GoloService, Upload) {
        /* jshint validthis:true */
        var vm = this;
        vm.Api = GoloService;
        vm.ProfileImageUrl = "/images/overlay/user-no-thumb.jpg";
        vm.Edit = false;
        vm.PhotoEdit = false;
        vm.DataLoaded = false;
        vm.AccountInfo = {};
        vm.BackupInfo = {};
        vm.CheckingUsername = false;
        vm.CheckingEmail = false;
        vm.Message = "";
        vm.CheckUsername = _checkUsername;
        vm.CheckEmail = _checkEmail
        vm.CancelEdit = _cancelEdit;
        vm.SaveEdit = _saveEdit;
        vm.UploadClicked = _uploadClicked;
        vm.UploadPic = false;
        vm.PictureFile = null;
        vm.UploadedNewProfilePic = false;

        activate();

        function activate() {
           vm.Api.GetAccountInfoForCurrentUser();
        }

        function _checkUsername() {
            if (vm.DataLoaded) {
                if ((vm.AccountInfo.NickName != "") && (vm.AccountInfo.NickName != vm.BackupInfo.NickName)) {
                    vm.CheckingUsername = true;
                    vm.Api.CheckUsername(vm.AccountInfo.NickName);
                } else {
                    if (vm.AccountInfo.Username == vm.BackupInfo.Username) {
                        vm.accountForm.Username.$setValidity('existing', true);
                        vm.accountForm.Username.$setValidity('required', true);
                    }
                }
                
            }
            

        }
        function _checkEmail() {
            if (vm.DataLoaded) {
                if ((vm.AccountInfo.Email != "") && (vm.AccountInfo.Email != vm.BackupInfo.Email)) {
                    vm.CheckingEmail = true;
                    vm.Api.CheckEmail(vm.AccountInfo.Email);
                } else {
                    if (vm.AccountInfo.Email == vm.BackupInfo.Email) {
                        vm.accountForm.Email.$setValidity('existing', true);
                        vm.accountForm.Email.$setValidity('required', true);
                    }
                }
            }
        }
        function _cancelEdit(){
            if(vm.BackupInfo != {}){
                vm.AccountInfo = $.extend(true, {}, vm.BackupInfo);
               
            } else {
               vm.DataLoaded = false;
               vm.Api.GetAccountInfoForCurrentUser(); 
            }
            vm.Edit = false;
        }
        function _saveEdit(){
            vm.Api.UpdateAccountInfoForCurrentUser(vm.AccountInfo);
        }

        vm.uploadPic = function (file) {

            if (file == null) {
                return;
            }

            file.upload = Upload.upload({
                url: '/api/UploadProfilePicture',
                data: { file: file, id: vm.AccountInfo.Id },
            });

            file.upload.then(function (response) {
                if (response.data.Completed == true) {
                    Upload.dataUrl(file, true).then(function (data) {
                        vm.PhotoEdit = false;
                        vm.PictureFile = null;
                        vm.UploadedNewProfilePic = true;
                        vm.ProfileImageUrl = data;
                    });
                }
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
        function _uploadClicked(state) {
            if (state == 0) {
                vm.UploadPic = true;
            } else {
                vm.uploadPic(vm.PictureFile);
            }

        }

        $rootScope.$on(vm.Api.GetAccountInfoForCurrentUserHdlr, function (s, rtn) {
            vm.AccountInfo = rtn;
            if(vm.AccountInfo.Country == "CA"){
                vm.AccountInfo.Province = vm.AccountInfo.State;
                vm.AccountInfo.State = "";
            }
            vm.BackupInfo = $.extend(true, {}, rtn);
          
            vm.DataLoaded = true;

            vm.Api.GetUserProfilePicture(vm.AccountInfo.Id);
        });
        $rootScope.$on(vm.Api.UpdateAccountInfoForCurrentUserHdlr, function (s, rtn) {
            vm.Message = "Account information updated successfully.";
            vm.BackupInfo = $.extend(true, {}, vm.AccountInfo);
            vm.Edit = false;
        });

        $rootScope.$on(vm.Api.CheckEmailHdlr, function (s, rtn) {
            if (rtn == false) {
                vm.CheckingEmail = false;
                vm.accountForm.Email.$setValidity('existing', true);
                vm.accountForm.Email.$setValidity('required', true);
            } else {
                vm.accountForm.Email.$setValidity('existing', false);
                
                
            }
        });
        $rootScope.$on(vm.Api.CheckUsernameHdlr, function (s, rtn) {
            if (rtn == false) {
                vm.CheckingUsername = false;
                vm.accountForm.Username.$setValidity('existing', true);
                vm.accountForm.Username.$setValidity('required', true);
            } else {
                vm.accountForm.Username.$setValidity('existing', false);
               
                
            }
        });
       
        $scope.$on(vm.Api.getUserProfilePictureHdlr, function (s, rtn) {
            //var blob = new Blob([rtn], { type: 'image/png' });
            //vm.ProfileImageUrl = blob;
        });

    }
})();