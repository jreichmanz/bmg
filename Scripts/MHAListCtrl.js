﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('MHAListCtrl', MHAListCtrl);

    MHAListCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function MHAListCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'MHAListCtrl';
        vm.Api = GoloService;

        // Variables
        vm.MHAs = [];
        //vm.MHAs = [{ DateCreated: '1-1-2016', ID: 1 }, { DateCreated: '1-2-2016', ID: 2 }, { DateCreated: '1-3-2016', ID: 3 }];

        // Functions
        vm.ViewMHA = _viewMHA;

        activate();

        function activate() {
            //$rootScope.settings.pageTitle = "GOLO Metabolic Health Analysis";

            //vm.Api.ListMHAsForCurrentUser();
        }

        function _viewMHA(mha) {
            vm.Api.GetMHAPDF(mha.DateCreated, mha.ID);
        }

        $scope.$on(vm.Api.listMHAsForCurrentUserHdlr, function (s, rtn) {
            vm.MHAs = rtn;
        });

        $scope.$on(vm.Api.getMHSPDFHdlr, function (s, rtn) {
            if (rtn) {
                // Display the PDF in some way
            }
        });

    }
})();