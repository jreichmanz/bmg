﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('AnalysisCtrl', AnalysisCtrl);

    AnalysisCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function AnalysisCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'SurveyCtrl';
        vm.Api = GoloService;

        // Variables
        vm.FormData = {};
        vm.Step = 1;
        vm.YearError = false;
        vm.WeightError = false;
        vm.WaistError = false;

        // Functions
        vm.NextStep = _nextStep;
        vm.PrevStep = _prevStep;
        vm.Save = _save;
        vm.First = true;
        vm.ShowFrameSizeInfo = false;
        vm.ShowActivityLevelInfo = false;
        vm.Saving = false;
        vm.CheckMhaCount = _checkMhaCount;


        function activate() {
            $('#Top_Right').hide();
            //$rootScope.settings.pageTitle = "GOLO Metabolic Health Analysis";
            vm.FormData.SEX = 'Male';
            vm.FormData.HEIGHT_FT = '4';
            vm.FormData.HEIGHT_IN = '0';
            vm.FormData.FRAMESIZE = 'Small';
            vm.FormData.ACTIVITY = 'Sedentary';
            vm.FormData.PUSHUPLIST = 'Zero';
            vm.FormData.STAIRSLIST = 'None';
            vm.FormData.EXERCISEHOURLIST = 'None';
            vm.FormData.WEIGHTGAINHISTORY = 'Yes';
            vm.FormData.DIETCOUNT = 'None';
            vm.FormData.BARCOUNT = 'None';
            vm.FormData.PROCESSEDFOODLIST = 'None';
            vm.FormData.SODACOUNT = 'None';
            vm.FormData.STRESSEATER = 'No';
            vm.FormData.EMOTIONALEATER = 'No';
            
            if (ProductPageCtrl == undefined)
            {
                // wait
                setTimeout(vm.CheckMhaCount, 1000);
            }
            else if (ProductPageCtrl > 0) {
                vm.First = false;
            }
        }

        function _checkMhaCount()
        {
            if (ProductPageCtrl == undefined) {
                // wait
                setTimeout(vm.CheckMhaCount, 1000);
            }
            else if (ProductPageCtrl > 0) {
                vm.First = false;
            }
        }
        
        function _nextStep() {

            if (vm.Step == 1) {
                if ((vm.FormData.YEAR == "" || vm.FormData.YEAR === undefined) || (vm.FormData.WEIGHT === "" || vm.FormData.WEIGHT === undefined) || (vm.FormData.WAISTSIZE === "" || vm.FormData.WAISTSIZE === undefined)) {

                    if (ProductPageCtrl == 0)
                    {
                        if (vm.FormData.YEAR == "" || vm.FormData.Year === undefined) {
                            vm.YearError = true;
                        }
                    }
                    if (vm.FormData.WEIGHT === "" || vm.FormData.WEIGHT === undefined) {
                        vm.WeightError = true;
                    }

                    if (vm.FormData.WAISTSIZE === "" || vm.FormData.WAISTSIZE === undefined) {
                        vm.WaistError = true;
                    }

                    if (vm.YearError || vm.WeightError || vm.WaistError) {
                        return;
                    }

                }

                vm.YearError = false;
                vm.WeightError = false;
                vm.WaistError = false;
            }

            vm.Step++;
        }

        function _prevStep() {
            vm.Step--;
        }

        function _save() {
            vm.Saving = true;
            vm.Api.CompleteHealthAnalysis(vm.FormData);
        }

        $scope.$on(vm.Api.completeHealthAnalysisHdlr, function (s, rtn) {
            if (rtn) {
                // Success
                window.location.href = "/default.aspx";
            } else {
               
                // Failed
            }
        });

        $scope.$on("$destroy", function () {
            $('#Top_Right').show();
        });

        activate();
    }
})();