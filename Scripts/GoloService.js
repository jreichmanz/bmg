﻿(function() {

    angular
        .module('GoloApp')
        .factory('GoloService', GoloService);

    GoloService.$inject = ['$http', '$rootScope'];

    function GoloService($http, $rootScope) {


        var _t = {

            // Route Config
            Api: 'api/',

            // Stored variables

            // Functions
            Login: _login,
            loginHdlr: "login.complete",

            CompleteHealthAnalysis: _completeHealthAnalysis,
            completeHealthAnalysisHdlr: 'mha.complete',

            GetMHAPDF: _getMHAPDF,
            getMHSPDFHdlr: 'mha.get.pdf',

            ListMHAsForCurrentUser: _listMHAsForCurrentUser,
            listMHAsForCurrentUserHdlr: 'mha.list.user',

            GetMetabolicRateForCurrentUser: _getMetabolicRateForCurrentUser,
            getMetabolicRateForCurrentUserHdlr: 'mha.get.metabolicrate',


            GetAccountInfoForCurrentUser: _getAccountInfoForCurrentUser,
            GetAccountInfoForCurrentUserHdlr: 'currentuser.get.accountinfo',

            UpdateAccountInfoForCurrentUser: _updateAccountInfoForCurrentUser,
            UpdateAccountInfoForCurrentUserHdlr: 'currentuser.update.accountinfo',

            GetCurrentUserName: _getCurrentUserName,
            GetCurrentUserNameHdlr: 'currentuser.get.firstname',

            CheckUsername: _checkUsername,
            CheckUsernameHdlr: 'account.username.check',

            CheckEmail: _checkEmail,
            CheckEmailHdlr: 'account.email.check',

            GenerateMHADataSummary: _generateMHADataSummary,
            generateMHADataSummaryHdlr: 'mha.generate.summary',

            GenerateOverallMHADataSummary: _generateOverallMHADataSummary,
            generateOverallMHADataSummaryHdlr: 'mha.generate.overallsummary',

            ListPreviousFiveMetabolicRates: _listPreviousFiveMetabolicRates,
            listPreviousFiveMetabolicRatesHdlr: 'mha.list.pmr.previousfive',

            // Fuel Tracking

            GetFuelTrackingByWeek: _getFuelTrackingByWeek,
            getFuelTrackingByWeekHdlr: 'fueltracking.get.week',

            GetFuelTrackingByWeekAndYear: _getFuelTrackingByWeekAndYear,
            getFuelTrackingByWeekAndYearHdlr: 'fueltracking.get.weekyear',

            UpdateWeekFuelTracking: _updateWeekFuelTracking,
            updateWeekFuelTrackingHdlr: 'fueltracking.update',

            // Fuel Track Week

            AddFuelTrackWeek: _addFuelTrackWeek,
            addFuelTrackWeekHdlr: 'fueltrackweek.add',

            UpdateFuelTrackWeek: _updateFuelTrackWeek,
            updateFuelTrackWeekHdlr: 'fueltrackweek.update',

            GetFuelTrackWeekByWeekAndYear: _getFuelTrackWeekByWeekAndYear,
            getFuelTrackWeekByWeekAndYearHdlr: 'fueltrackweek.getby.weekyear',

            ListPreviousFiveFuelTrackWeek: _listPreviousFiveFuelTrackWeek,
            listPreviousFiveFuelTrackWeekHdlr: 'fueltrackweek.list.previousfive',

            // Progress Tracking

            AddProgressTrack: _addProgressTrack,
            addProgressTrackHdlr: 'progresstrack.add',

            UpdateProgressTrack: _updateProgressTrack,
            updateProgressTrackHdlr: 'progresstrack.update',

            DeleteProgressTrack: _deleteProgressTrack,
            deleteProgressTrackHdlr: 'progresstrack.delete',

            ListProgressTrackByUser: _listProgressTrackByUser,
            listProgressTrackByUserHdlr: 'progresstrack.list.user',

            // User

            GetUserProfilePicture: _getUserProfilePicture,
            getUserProfilePictureHdlr: 'user.get.profilepic',

            CheckIfUserHasDetails: _checkIfUserHasDetails,
            checkIfUserHasDetailsHdlr: 'user.check.details',

	    GetUserDetails: _getUserDetails,
            getUserDetailsHdlr: 'user.get.details',


            CurrentMHAs: "",

            //New MHA & Emotional Eating Quiz

            SaveMHAData: _saveMHAData,
            saveMHADataHdlr: 'mha.data.save',

            GetLatestFullMHAs: _getLatestFullMHAs,
            getLatestFullMHAsHdlr: 'mha.get.full'
        };
        _t.UserName = "";

        return _t;

        function _login(username, password) {
            //$http.post("/api/SaveMHAData", { MHAData: mhaData, EmotionalEatingData: eeqData, UserDetails: userDetails }).
            $http.post(_t.Api + "token", { Username: username, Password: password }).
                then(function (rtn) {
                    $rootScope.$broadcast(_t.loginHdlr, rtn.data);
                }, function (response) {
                    alert("There was an internal error. please log off and try again!");
                });

        }
        //New MHA & Emotional Eating Quiz

        //function _saveMHAData(mhaData, eeqData, userDetails) {
        function _saveMHAData(mhaData, userDetails) {
            //$http.post("/api/SaveMHAData", { MHAData: mhaData, EmotionalEatingData: eeqData, UserDetails: userDetails }).
            $http.post(_t.Api + "SaveMHAData", { MHAData: mhaData, UserDetails: userDetails }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.saveMHADataHdlr, rtn.data);
            }, function(response) {
                alert("There was an internal error. please log off and try again!");
            });

        }

        function _getLatestFullMHAs() {
            $http.post(_t.Api + "GetPreviousFiveFullMHAs", {}).
            then(function(rtn) {
                $rootScope.$broadcast(_t.getLatestFullMHAsHdlr, rtn.data);
            }, function(response) {
                alert("There was an internal error. please log off and try again!");
            });
        }

        //Old MHA
        function _completeHealthAnalysis(formData) {

            $http.post(_t.Api + 'CreateMHARecords', { "FormData": formData }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.completeHealthAnalysisHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _getMHAPDF(date, mhaID) {
            $http.get(_t.Api + 'GetFile', { "Date": date, "MHAID": mhaID }).

            then(function(rtn) {
                $rootScope.$broadcast(_t.getMHSPDFHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }



        function _listMHAsForCurrentUser() {
            $http.post(_t.Api + 'GetPreviousFiveFullMHAs').
            then(function(rtn) {
                $rootScope.$broadcast(_t.listMHAsForCurrentUserHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $rootScope.$broadcast(_t.listMHAsForCurrentUserHdlr, []);
            });
        }

        function _generateMHADataSummary() {
            $http.post(_t.Api + 'GenerateMHADataSummary').
            then(function(rtn) {
                $rootScope.$broadcast(_t.generateMHADataSummaryHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _generateOverallMHADataSummary(fromDate, toDate) {
            $http.post(_t.Api + 'GetAllMHASummary', { "FromDate": fromDate, "ToDate": toDate }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.generateOverallMHADataSummaryHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _listPreviousFiveMetabolicRates() {
            $http.post(_t.Api + 'GetPreviousFiveMetabolicRate').
            then(function(rtn) {
                $rootScope.$broadcast(_t.listPreviousFiveMetabolicRatesHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        // Fuel Tracking

        function _getFuelTrackingByWeek(week) {
            $http.post(_t.Api + 'fueltrack/GetByWeek', { "Week": week }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.getFuelTrackingByWeekHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _getFuelTrackingByWeekAndYear(week, year) {
            $http.post(_t.Api + 'fueltrack/GetByWeekAndYear', { "Week": week, "Year": year }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.getFuelTrackingByWeekAndYearHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _updateWeekFuelTracking(date, days, guid) {
            $http.post(_t.Api + 'fueltrack/Update', { "Date": date, "Days": days, "Guid": guid }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.updateWeekFuelTrackingHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        // Fuel Track Week

        function _addFuelTrackWeek(fuelTrackWeek) {
            $http.post(_t.Api + 'fueltrackweek/Add', fuelTrackWeek).
            then(function(rtn) {
                $rootScope.$broadcast(_t.addFuelTrackWeekHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _updateFuelTrackWeek(fuelTrackWeek) {
            $http.post(_t.Api + 'fueltrackweek/Update', fuelTrackWeek).
            then(function(rtn) {
                $rootScope.$broadcast(_t.updateFuelTrackWeekHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _getFuelTrackWeekByWeekAndYear(week, year) {
            $http.post(_t.Api + 'fueltrackweek/GetByWeekAndYear', { "Week": week, "Year": year }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.getFuelTrackWeekByWeekAndYearHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _listPreviousFiveFuelTrackWeek(week, year) {
            $http.post(_t.Api + 'fueltrackweek/ListPreviousFive', { "Week": week, "Year": year }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.listPreviousFiveFuelTrackWeekHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        // Progress Trackng

        function _addProgressTrack(progress) {
            $http.post(_t.Api + 'progresstrack/Add', progress).
            then(function(rtn) {
                $rootScope.$broadcast(_t.addProgressTrackHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _updateProgressTrack(progress) {
            $http.post(_t.Api + 'progresstrack/Update', progress).
            then(function(rtn) {
                $rootScope.$broadcast(_t.updateProgressTrackHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _deleteProgressTrack(progress) {
            $http.post(_t.Api + 'progresstrack/Delete', progress).
            then(function(rtn) {
                $rootScope.$broadcast(_t.deleteProgressTrackHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _listProgressTrackByUser(userID) {
            $http.post(_t.Api + 'progresstrack/ListByUser', { "UserID": userID }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.listProgressTrackByUserHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again!");
            });
        }

        function _getMetabolicRateForCurrentUser() {
            if (_t.CurrentMHAs != "") {
                $rootScope.$broadcast(_t.getMetabolicRateForCurrentUserHdlr, _t.CurrentMHAs);

            } else {
                $http.post(_t.Api + 'GetMetabolicRateForCurrentUser').
                then(function(rtn) {
                    _t.CurrentMHAs = rtn.data;
                    if (rtn.data != "") {
                        ProductPageCtrl = 1;
                    } else {
                        ProductPageCtrl = 0;
                    }
                    $rootScope.$broadcast(_t.getMetabolicRateForCurrentUserHdlr, _t.CurrentMHAs);
                    //$rootScope.$broadcast(_t.getMetabolicRateForCurrentUserHdlr, _t.CurrentMHAs);
                }, function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    _t.CurrentMHAs = "";
                    ProductPageCtrl = 0;
                    $rootScope.$broadcast(_t.getMetabolicRateForCurrentUserHdlr, []);
                });
            }

        }

        function _checkEmail(email) {
            $http.post(_t.Api + 'CheckIfEmailExists', { Email: email }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.CheckEmailHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again! accountinfo");
            });
        }

        function _checkUsername(username) {
            $http.post(_t.Api + 'CheckIfUsernameExists', { Username: username }).
            then(function(rtn) {
                $rootScope.$broadcast(_t.CheckUsernameHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again! accountinfo");
            });
        }

        function _getAccountInfoForCurrentUser() {
            $http.post(_t.Api + 'GetAccountInfoForCurrentUser').
            then(function(rtn) {
                $rootScope.$broadcast(_t.GetAccountInfoForCurrentUserHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again! accountinfo");
            });
        }

        function _updateAccountInfoForCurrentUser(accountInfo) {

            $http.post(_t.Api + 'UpdateAccountInfoForCurrentUser', accountInfo).
            then(function(rtn) {
                _t.UserName = rtn.data.NickName
                $rootScope.$broadcast(_t.UpdateAccountInfoForCurrentUserHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again! accountinfo");
            });
        }

        function _getCurrentUserName() {
            if (_t.UserName != "") {
                $rootScope.$broadcast(_t.GetCurrentUserNameHdlr, _t.UserName);
                return;
            }
            $http.post(_t.Api + 'GetCurrentUserName').
            then(function(rtn) {
                _t.UserName = rtn.data;
                $rootScope.$broadcast(_t.GetCurrentUserNameHdlr, _t.UserName);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                _t.UserName = "";
                $rootScope.$broadcast(_t.GetCurrentUserNameHdlr, _t.UserName);
            });
        }

        // User
        function _checkIfUserHasDetails() {
            $http.post(_t.Api + 'CheckUserDetails').
            then(function(rtn) {
                $rootScope.$broadcast(_t.checkIfUserHasDetailsHdlr, rtn.data);
            }, function(rtn) {
                $rootScope.$broadcast(_t.checkIfUserHasDetailsHdlr, false);
            });
        }
         function _getUserDetails() {
            $http.post(_t.Api + 'GetUserDetails').
            then(function(rtn) {
                $rootScope.$broadcast(_t.getUserDetailsHdlr, rtn.data);
            }, function(rtn) {
               // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again! userdetails");
            });
        }

        function _getUserProfilePicture(userGuid) {
            $http({
                method: 'POST',
                url: _t.Api + 'GetProfilePicture',
                data: 'contactID=' + userGuid,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                responseType: "blob"
            }).then(function(rtn) {
                $rootScope.$broadcast(_t.getUserProfilePictureHdlr, rtn.data);
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("There was an internal error. please log off and try again! accountinfo");
            });



            //, "id=" + userGuid).
            //then(function (rtn) {
            //    $rootScope.$broadcast(_t.getUserProfilePictureHdlr, rtn.data);
            //}, function (response) {
            //    // called asynchronously if an error occurs
            //    // or server returns response with an error status.
            //    alert("There was an internal error. please log off and try again! accountinfo");
            //});

            //$http.post(_t.Api + 'GetProfilePicture', "id=" + userGuid ).
            //    then(function (rtn) {
            //        $rootScope.$broadcast(_t.getUserProfilePictureHdlr, rtn.data);
            //    }, function (response) {
            //        // called asynchronously if an error occurs
            //        // or server returns response with an error status.
            //        alert("There was an internal error. please log off and try again! accountinfo");
            //    });
        }
    };
})();
