﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('PdfCtrl', PdfCtrl);

    PdfCtrl.$inject = ['$scope', '$rootScope', '$http', 'GoloService'];

    function PdfCtrl($scope, $rootScope, $http, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.Api = GoloService;
        vm.MHAInfo = null;
        vm.PDFUrl = "";
        vm.DownloadPDF = _downloadPDF;

        activate();

        function activate() {
            vm.Api.ListMHAsForCurrentUser();
        }
       
        function _downloadPDF() {
            var url = window.location.origin + encodeURI(vm.PDFUrl);

            var xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.responseType = "blob";
            xhr.overrideMimeType("octet/stream");
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var blob = new Blob([xhr.response], { type: 'application/pdf' });
                    var blobUrl = URL.createObjectURL(blob);

                    if (window.navigator.msSaveBlob) {
                        window.navigator.msSaveBlob(blob, 'Metabolic-Health-Analysis.pdf');
                    } else {
                        var hiddenElement = document.createElement('a');
                        hiddenElement.href = blobUrl;
                        hiddenElement.target = '_blank';
                        hiddenElement.download = 'Metabolic-Health-Analysis.pdf';
                        document.body.appendChild(hiddenElement);
                        hiddenElement.click();

                        setTimeout(function () {
                            document.body.removeChild(hiddenElement);
                            URL.revokeObjectURL(blobUrl);
                        }, 300);
                    }
                }
            };
            xhr.send();
        }

        $rootScope.$on('mha.list.user', function (s, rtn) {
            if (rtn.length == 0) { return;}
            vm.MHAInfo = rtn[0];
            var tempdate = new Date(vm.MHAInfo.DateCreated);
            var date = tempdate.toLocaleDateString().replace("/", "-").replace("/", "-");
            //remove when controller fixed
           
            vm.PDFUrl = '/api/GetFile/?Date=' + date + '&MHAID=' + vm.MHAInfo.ID;
            //Controller not returning file - uncomment when controller is fixed remove code above
            //vm.Api.GetMHAPDF(date, vm.MHAInfo.ID);
        });

        $rootScope.$on('mha.get.pdf', function (s, rtn) {
            var blob = new Blob([rtn]);
            document.getElementById("PDFDownload").download = "Metabolic-Health-Analysis.pdf";
            vm.PDFUrl = window.URL.createObjectURL(blob);
        });
        $scope.$watch(function () {
	return $rootScope.PDFAvailable;
	}, function() {
	vm.PDFAvailable = $rootScope.PDFAvailable;
	}, true);
    }
})();
