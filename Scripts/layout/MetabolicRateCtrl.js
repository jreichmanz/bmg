﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('MetabolicRateCtrl', MetabolicRateCtrl);

    MetabolicRateCtrl.$inject = ['$scope', '$rootScope', 'GoloService'];

    function MetabolicRateCtrl($scope, $rootScope, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.Api = GoloService;
        vm.MetabolicRate = "";
        vm.Activate = _activate;
        vm.LoggedIn = false;

        function _activate() {
            // if(vm.Api.)
            vm.Api.GetMetabolicRateForCurrentUser();
            vm.Api.Login("sreichman@bmgdigitaltech.com", "@G0lOaCc3ss0nly!L8r");
        }

        //Returns and displays metabolic rate.
        $rootScope.$on('mha.get.metabolicrate', function (s, rtn) {
            vm.MetabolicRate = rtn;
        });
        $rootScope.$on(vm.Api.loginHdlr, function (s, rtn) {
            vm.LoggedIn = true;
        });


        vm.Activate();
    }
})();