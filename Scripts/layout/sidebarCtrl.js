﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('SidebarCtrl', SidebarCtrl);

    SidebarCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService', '$state'];

    function SidebarCtrl($scope, $rootScope, $location, GoloService, $state) {
        /* jshint validthis:true */
        var vm = this;
        
        vm.currentstate = "";
        vm.checkSubMenu = _checkSubMenu;
        vm.checkActive = _checkactive;
        activate();

        function activate() {
            vm.currentstate = $state.current.name;
        }

        function _checkSubMenu(state) {
            if(vm.currentstate.indexOf(state) != -1){
                return true;
            }
            return false;
        }

        function _checkactive(state) {
            if(state == vm.currentstate){
                return true;
            }
            return false;
        }

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){ 
            vm.currentstate = toState.name;
        });

    }
})();