﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('MHAResultsCtrl', MHAResultsCtrl);

    MHAResultsCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function MHAResultsCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.Api = GoloService;
        vm.MetabolicRate = "";
      
        activate();

        function activate() {
          vm.Api.GetMetabolicRateForCurrentUser();
        }

        //Change to the correct even for when the metabolic rate is returned.
        $rootScope.$on('mha.get.metabolicrate', function (s, rtn) {
            vm.MetabolicRate = rtn;
        });

    }
})();