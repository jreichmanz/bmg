﻿(function() {
    'use strict';

    angular
        .module('GoloApp')
        .controller('PageHeaderCtrl', PageHeaderCtrl);

    PageHeaderCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService', '$state'];

    function PageHeaderCtrl($scope, $rootScope, $location, GoloService, $state) {
        /* jshint validthis:true */
        var vm = this;

        vm.currentstate = "";
        vm.ImageUrl = ""
        vm.AltText = ""
        vm.setHeader = _setHeader;
        activate();

        function activate() {
            vm.currentstate = $state.current.name;
            vm.setHeader();
        }

        function _setHeader() {
            switch (vm.currentstate) {
                case "progresstracking":
                    vm.AltText = "Your Best You. Starting Right Now... and never ending";
                    vm.ImageUrl = "images/headers/banner-3.jpg";
                    break;
                case "fueltracking":
                    vm.AltText = "Your Best You. Starting Right Now... and never ending";
                    vm.ImageUrl = "images/headers/banner-3.jpg";
                    break;
                case "healthanalysis":
                    vm.AltText = "Metabolic Health Analysis";
                    vm.ImageUrl = "images/bck-top-health-analisis.jpg";
                    break;
                default:
                    vm.AltText = "Your Best You. Starting Right Now... and never ending"
                    vm.ImageUrl = "images/headers/banner-4.jpg"
                    break;
            }
        }

        function _checkactive(state) {
            if (state == vm.currentstate) {
                return true;
            }
            return false;
        }

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
            vm.currentstate = toState.name;
            vm.setHeader();
        });

    }
})();
