﻿(function() {
    'use strict';

    angular
        .module('GoloApp')
        .controller('TopHeaderCtrl', TopHeaderCtrl);

    TopHeaderCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService', '$state', '$window'];

    function TopHeaderCtrl($scope, $rootScope, $location, GoloService, $state, $window) {
        /* jshint validthis:true */
        var vm = this;
        vm.Api = GoloService;
        vm.Welcome = "";
        vm.Activate = _activate;
        vm.CheckCount = 0;

        function _activate() {

            if (vm.Api.UserName != "") {
                vm.Welcome = "Welcome, " + vm.Api.UserName;
            } else {
                vm.CheckCount = vm.CheckCount + 1;
                if (vm.CheckCount < 5) {
                    setTimeout(function() { vm.Activate(); }, 1000);
                } else {
                    vm.Api.GetCurrentUserName();
                }
            }
        }


        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options) {
            $window.scrollTo(0, 0);
        });
        $rootScope.$on(vm.Api.GetCurrentUserNameHdlr, function(s, rtn) {
            vm.Welcome = "Welcome, " + rtn;
        });
        vm.Activate();
    }
})();