﻿(function() {
    'use strict';

    angular
        .module('GoloApp')
        .controller('MHAQuestionnaireCtrl', MHAQuestionnaireCtrl);

    MHAQuestionnaireCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function MHAQuestionnaireCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'SurveyCtrl';
        vm.Api = GoloService;

        // Variables
        vm.Data = {
            MHAData: {
                ExcercisePerWeek: '0',
                MealReplacementsPerWeek: '0',
                ProcessedFoodsPerWeek: '0',
                SodasPerWeek: '0',
                EmotionalEatingRating: 1,
                CravingsRating: 1,
                SleepRating: 1,
                EnergyRating: 1,
                MoodRating: 1,
                StressRating: 1
            },
            EEQData: {
                E01: 0,
                S13: 0,
                C21: 0,
                I37: 0,
                E07: 0,
                I35: 0,
                C27: 0,
                S15: 0,
                E04: 0,
                C25: 0,
                I38: 0,
                E05: 0,
                S11: 0,
                C22: 0,
                I32: 0,
                S18: 0,
                E08: 0,
                C24: 0,
                I36: 0,
                C29: 0,
                E03: 0,
                S12: 0,
                I34: 0,
                E02: 0,
                S16: 0,
                C23: 0,
                I31: 0,
                C26: 0,
                E09: 0,
                S14: 0,
                I40: 0,
                S17: 0,
                C30: 0,
                E06: 0,
                S19: 0,
                I33: 0,
                C28: 0,
                S20: 0,
                E10: 0,
                I39: 0
            },
            UserDetails: {
                Sex: 0,
                HeightFT: '4',
                HeightIN: '0'
            }
        };
        vm.Step = 1;
        vm.YearError = false;
        vm.WeightError = false;
        vm.WaistError = false;

        // Functions
        vm.NextStep = _nextStep;
        vm.PrevStep = _prevStep;
        vm.Save = _save;
        vm.First = true;
        vm.ShowFrameSizeInfo = false;
        vm.ShowActivityLevelInfo = false;
        vm.Saving = false;
        ////vm.CheckMhaCount = _checkMhaCount;


        function activate() {
            //vm.Api.CheckIfUserHasDetails();
            $('#Top_Right').hide();
            //$rootScope.settings.pageTitle = "GOLO Metabolic Health Analysis";

            //New Checking for MHA Data

            //Old Checking for MHA Data
            //if (window.MhaCount == undefined) {
            //    // wait
            //    setTimeout(vm.CheckMhaCount, 1000);
            //}
            //else if (window.MhaCount > 0) {
            //    vm.First = false;
            //}
            $(".bs-slider").slider();
            $('.slider').on("change", function(e) {
                var input = $(this).next("input");
                var value,
                    strings = input.attr("ng-model").split("."),
                    controllerValue = vm;
                switch (input.attr("type")) {
                    case "number":
                        value = parseInt(input.val());
                        break;
                    default:
                        value = input.val();
                }
                strings.forEach(function(object, index, objects) {
                    if (index > 0) {
                        if (index == (objects.length - 1)) {
                            controllerValue[object] = value;
                        } else {
                            var currentObject = controllerValue[object];
                            controllerValue = currentObject;
                        }
                    }
                })
            });
        }

        //function _checkMhaCount() {
        //    if (window.MhaCount == undefined) {
        //        // wait
        //        setTimeout(vm.CheckMhaCount, 1000);
        //    }
        //    else if (window.MhaCount > 0) {
        //        vm.First = false;
        //    }
        //}

        function _nextStep() {
            switch (vm.Step) {
                case 1:
                    if ((vm.Data.UserDetails.Year == "" || vm.Data.UserDetails.Year === undefined) || (vm.Data.MHAData.Weight === "" || vm.Data.MHAData.Weight === undefined) || (vm.Data.MHAData.Waist === "" || vm.Data.MHAData.Waist === undefined)) {

                        if (window.MhaCount == 0) {
                            if (vm.Data.UserDetails.Year == "" || vm.Data.UserDetails.Year === undefined) {
                                vm.YearError = true;
                            }
                        }
                        if (vm.Data.MHAData.Weight == "" || vm.Data.MHAData.Weight === undefined) {
                            vm.WeightError = true;
                        }

                        if (vm.Data.MHAData.Waist == "" || vm.Data.MHAData.Waist === undefined) {
                            vm.WaistError = true;
                        }

                        if (vm.YearError || vm.WeightError || vm.WaistError) {
                            return;
                        }

                    }

                    vm.YearError = false;
                    vm.WeightError = false;
                    vm.WaistError = false;
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;

            }

            vm.Step++;
        }

        function _prevStep() {
            vm.Step--;
        }

        function _save() {
            vm.Saving = true;
            //vm.Api.SaveMHAData(vm.Data.MHAData, vm.Data.EEQData, vm.Data.UserDetails);
            vm.Api.SaveMHAData(vm.Data.MHAData, vm.Data.UserDetails);
        }

        $scope.$on(vm.Api.saveMHADataHdlr, function(s, rtn) {
            if (rtn) {
                // Success
                window.location.href = "#/";
            } else {
                vm.Saving = false;
                alert("There was an error processing your questionnaire - Please verify your answers are correct, and try again. If the problem persists, please contact support.")
                    // Failed
            }
        });

        $scope.$on(vm.Api.checkIfUserHasDetailsHdlr, function(s, rtn) {
            if (rtn) {
                vm.First = false;
            }
            vm.Step++;
        });

        $scope.$on("$destroy", function() {
            $('#Top_Right').show();
        });

        activate();
    }
})();