﻿(function() {
    'use strict';

    angular
        .module('GoloApp')
        .controller('MHAResultsPrintCtrl', MHAResultsPrintCtrl);

    MHAResultsPrintCtrl.$inject = ['$scope', '$rootScope', '$location', 'GoloService'];

    function MHAResultsPrintCtrl($scope, $rootScope, $location, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        // Variables
        vm.title = 'MHAResultsPrintCtrl';
        vm.Api = GoloService;
        vm.MHA = {};
        vm.UserDetails = {};
        vm.Name = vm.Api.UserName;




        // Functions
        window.OpenPDF = _openPDF;

        activate();

        function activate() {
        	$rootScope.results = true;
            vm.Api.GetCurrentUserName();
            if (!vm.Api.MHAResults) {
                vm.Api.GetLatestFullMHAs();
                vm.Api.GetUserDetails();

            } else {
                vm.MHA = vm.Api.MHAREsults;
            }

        }

        function _openPDF() {
            var doc = new jsPDF();
            var html = $('.pdf-print')[0];
            doc.addHTML(html, function() {
                doc.save('mha.pdf');
            });

        }

        $scope.$on(vm.Api.getLatestFullMHAsHdlr, function(s, rtn) {
            vm.MHA = rtn[0];
        });
         $scope.$on(vm.Api.getUserDetailsHdlr, function(s, rtn) {
            vm.UserDetails = rtn;
        });
        $rootScope.$on(vm.Api.GetCurrentUserNameHdlr, function(s, rtn) {
            vm.Name = rtn;
        });

    }
})();