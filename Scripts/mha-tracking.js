﻿(function () {
    'use strict';

    angular
        .module('GoloApp')
        .controller('ResultsTrackingCtrl', ResultsTrackingCtrl);

    ResultsTrackingCtrl.$inject = ['$scope', '$rootScope', '$location', '$filter', 'GoloService'];

    function ResultsTrackingCtrl($scope, $rootScope, $location, $filter, GoloService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'ResultsTrackingCtrl';
        vm.Api = GoloService;
	vm.UserDetails = {};
        vm.Results = [];
        vm.Table = [];
        // Variables
       
        //Functions
        vm.SortForTable = _sortForTable;
        vm.InitTable = _initTable;
        vm.GetFrame = _getFrame;
        vm.GetActivity = _getActivity;
        vm.GetExcercise = _getExcercise;
        vm.GetProcessedFoodRating = _getProcessedFoodRating;
        vm.GetLifestyleRating = _getLifeStyleRating;
        vm.GetRatingGreatToVeryPoor = _getRatingGreatToVeryPoor;
        vm.GetRatingLowToHigh = _getRatingLowToHigh;

        activate();

     

        function activate() {
           vm.Api.GetUserDetails();
        }


        function _initTable() {
            if (typeof vm.Table[0] === 'undefined') {
                vm.Table = [];
                var fields = [
                    "Date",

                    "What You Told Us",

                    "Weight",
                    "Height",
                    "Frame",
                    "Waist",
                    "Activity (non excercise)",
                    "Current hours excercise per week",
                    "Number of meal replacement shakes, bars, and meals per week",
                    "Number of processed foods per week",
                    "Number of sodas per week",
                    "Your stress and emotional eating rating",
                    "Your hunger and cravings between meals rating",
                    "Your sleep quality rating",
                    "Your energy level rating",
                    "Your mood and happiness rating",
                    "Your stress level rating",

                    "Your Assesment Results",

                    "Processed Food Score",
                    "Emotional Eating Score",
                    "Life Style Score",
                    "Estimated Current GOLO BMI",
                    "Ideal Weight",
                    "Ideal Weight Range",
                    "Ideal BMI",
                    "Ideal Weight to Lose",
                    "Percentage of Body Weight to Lose",
                    "First Weight Loss Goal",
                    "Percentage of Current Body Weight",
                    "Realistic Weight Loss Goal Per Week",
                    "First Waist Reduction Goal",
                    "Waist at First Weight Loss Goal",

                    "Personal Metabolic Rate",

                    "Daily Personal Metabolic Rate",
                    "Breakfast Guide",
                    "Lunch Guide",
                    "Dinner Guide",
                    "Starting Fit Points Goal (per week)",
                    "Fit Points Per Day",
                    "Personal Metabolic Rate (with Fit Points)"

                ];
                fields.forEach(function (field, index, fields) {
                    vm.Table[index] = [];
                    vm.Table[index].push(field);
                });
            }

           
        }
        function _sortForTable() {

            vm.InitTable();
            for (var i = 0; i < vm.Table.length;i++){
                vm.Results.forEach(function (result, resultIndex, results) {
                    var tempArray = [result.DateCreated, " ", result.Weight + " lbs.", vm.UserDetails.HeightFT + "' " + vm.UserDetails.HeightIN + "\"", vm.GetFrame(result.Frame), result.Waist + "\"", vm.GetActivity(result.Activity), vm.GetExcercise(result.ExcercisePerWeek), result.MealReplacementsPerWeek, result.ProcessedFoodsPerWeek, result.SodasPerWeek, result.EmotionalEatingRating + " - " +vm.GetRatingLowToHigh(result.EmotionalEatingRating), result.CravingsRating + " - " +vm.GetRatingLowToHigh(result.CravingsRating), result.SleepRating + " - " + vm.GetRatingGreatToVeryPoor(result.SleepRating), result.EnergyRating + " - " + vm.GetRatingGreatToVeryPoor(result.EnergyRating), result.MoodRating + " - " + vm.GetRatingGreatToVeryPoor(result.MoodRating), result.StressRating + " - " + vm.GetRatingLowToHigh(result.StressRating), " ", result.ProcessedFoodScore + " - " + vm.GetProcessedFoodRating(result.ProcessedFoodScore), result.EmotionalEatingScore + " - " + vm.GetRatingLowToHigh(result.EmotionalEatingScore), result.LifeStyleScore + " - " + vm.GetLifestyleRating(result.LifeStyleScore), result.EstimatedGoloBMI, result.IdealWeight + " lbs.", result.IdealWeightRange + " lbs.", result.IdealBMI, result.IdealWeightToLose + " lbs.", result.PercentageToLose + "%", result.WeightLossGoal + " lbs.", result.WeightLossGoalPercentage + "%", result.RealisticWeightLossGoalPerWeek + " lbs.", result.WaistReductionGoal + "\"", result.WaistAtWeightLossGoal + "\"", " ", result.PersonalMetabolicRate, result.BreakfastGuide, result.LunchGuide, result.DinnerGuide, result.FitPointsGoalPerWeek, result.FitPointsPerDay, result.PMRWithFitPoints]
                    vm.Table[i].push(tempArray[i]);
                });
            }
        }
        
        $scope.$on(vm.Api.getLatestFullMHAsHdlr, function (s, rtn) {
            vm.Results = rtn;
            vm.SortForTable();
            
        });
         $scope.$on(vm.Api.getUserDetailsHdlr, function(s, rtn) {
            vm.UserDetails = rtn;
            vm.Api.GetLatestFullMHAs();
        });
        function _getFrame(number){
        var result = "";
        	switch(number){
        		case 1.2:
        		result = "Large";
        		break;
        		case 1.1:
        		result = "Medium";
        		break;
        		default:
        		result = "Small";
        		break;
        	}
        	return result;
        }
        function _getActivity(number) {
        var result = "";
        	switch(number) {
        		case 20:
        		result = "Sedentary";
        		break;
        		case 35:
        		result = "Moderate";
        		break;
        		default:
        		result = "High";
        		break;
        	}
        	return result;
        }
        function _getExcercise(number){
         var result = "";
        	switch(number) {
        		case 0:
        		result = "None";
        		break;
        		case 20:
        		result = "1hr or Less";
        		break;
        		case 30:
        		result = "1-2 hrs";
        		break;
        		case 50:
        		result = "3-5 hrs";
        		break;
        		default:
        		result = "6+ hrs";
        		break;
        	}
        	return result;
        
        }
        
        function _getRatingLowToHigh(number){
        var result = "";
        	if(number ==1 || number ==2) {
        		result = "Good";
        	} else if (number == 3 || number == 4) {
        		result = "Low";        	
        	} else if (number == 5 || number == 6) {
        	        		result = "Mild";
        	}else if (number == 7 || number == 8) {
        	        		result = "Moderate";
        	} else {
        	        		result = "High";
        	}
        	        	return result;
	}
	function _getRatingGreatToVeryPoor(number){
        var result = "";
        	if(number ==1 || number ==2) {
        	        		result = "Great";
        	} else if (number == 3 || number == 4) {
        	        		result = "Good";
        	} else if (number == 5 || number == 6) {
        	        		result = "Average";
        	}else if (number == 7 || number == 8) {
        	        		result = "Poor";
        	} else {
        	        		result = "Very Poor";
        	}
        	return result;
	}
	function _getLifeStyleRating(number){
	var result = "";
        	if(number < 1 ) {
        	        		result = "Great";
        	} else if (number > 0 && number < 11) {
        	        		result = "Good";
        	} else if (number > 10 && number < 20) {
        	        		result = "Average";
        	}else if (number > 20 && number < 40) {
        	        		result = "Poor";
        	} else {
        	        		result = "Very Poor";
        	}
        return result;
	
	}
	function _getProcessedFoodRating(number){
	var result = "";
        	if(number < 6 ) {
        	        		result = "Good";
        	} else if (number > 5 && number < 11) {
        	        		result = "Low";        	
        	} else if (number > 10 && number < 16) {
        	        		result = "Mild";
        	}else if (number > 15 && number < 25) {
        	        		result = "Moderate";
        	} else {
        	        		result = "High";
        	}
        return result;
	
	}
        

      
       
    }
})();