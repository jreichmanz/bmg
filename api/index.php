<?php
// /API/index.php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \golo\models;
use \golo\utilities;
use Slim\App;
use Slim\Middleware\TokenAuthentication;
use Slim\Middleware\TokenAuthentication\UnauthorizedExceptionInterface;
use golo\Encryption as Encryption;
use golo\Bearer as Bearer;
use golo\AccessToken as AccessToken;
use golo\debug as debug;

use golo\ApiUtilities\Token as Token;

// use golo\debug as debug;
//Models
use golo\models\AuthClient as AuthClient;
use golo\models\RefreshToken as RefreshToken;
use golo\models\MHAFull as MHAFull;
use golo\models\wrappers\PersonalMetabolicRate as PersonalMetabolicRate;
use golo\models\EmotionalEatingData as EmotionalEatingData;
use golo\models\FuelTrack as FuelTrack;
use golo\models\FuelTrackWeek as FuelTrackWeek;
use golo\models\MHAData as MHAData;
use golo\models\MHAResults as MHResults;
use golo\models\ProgressTrack as ProgressTrack;
use golo\models\UserDetails as UserDetails;
//Utilities
use golo\utilities\AuthenticationUtility as AuthenticationUtility;
use golo\utilities\FuelTrackUtility as FuelTrackUtility;
use golo\utilities\FuelTrackWeekUtility as FuelTrackWeekUtility;
use golo\utilities\MHAUtility as MHAUtility;
use golo\utilities\ProgressTrackUtility as ProgressTrackUtility;
use golo\utilities\UserDetailsUtility as UserDetailsUtility;


require '../vendor/autoload.php';
require '../goloContext.php';

require_once '../../app/Mage.php';

class UnauthorizedException extends \Exception implements UnauthorizedExceptionInterface
{
    
}

//Slim framework config
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

//Slim token authentication middleware

$app = new App(["settings" => $config]);
$container = $app->getContainer();
$userId = 0;

function SetContainer($uid,$name){
    $GLOBALS["container"]["userId"] = $uid;
    shell_exec('wall test userid '.$userId.' usreid '.$uid);
}


$authenticator = function($request, TokenAuthentication $tokenAuth){
    $GLOBALS["container"]["debug"]->log("authentication start");
    try{
        Mage::app('mygolo');
        Mage::getSingleton('core/session', array('name' => 'frontend'));
        if(Mage::getSingleton('customer/session')->isLoggedIn()){
            shell_exec('wall magento authentication');
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $userId = $customer->getId();
            $name = $customer->getFirstname();
            SetContainer($userId);
        } else {
            $encryption = new Encryption();
            $GLOBALS["container"]["debug"]->log("authentication else start ");
            
            $bearer = new Bearer($request->getHeader("authorizeData")[0]);
            $secret = $request->getHeader('Secret')[0];
            $token = new Token();
           
            $userId = $token->GetUserIdFromToken($bearer->getAccessToken(),$secret,$encryption->decrypt($secret,$request->getHeader('clientID')[0]));
            SetContainer($userId);
            $GLOBALS["container"]["debug"]->log("authentication else end uid ".$userId);
        }
    } catch(Exception $e){
        $GLOBALS["container"]["debug"]->log("authentication exception thrown 401 issued. \"".$e."\"");
        return $response->withStatus(401);
    }
};

$app->add(new TokenAuthentication([
'path' => ['/fueltrack', '/fueltrackweek', '/progresstrack', '/GetMetabolicRateForCurrentUser' , '/GetPreviousFiveFullMHAs', '/GetPreviousFiveMetabolicRate', '/SaveMHAData', '/CheckUserDetails', '/GetUserDetails', '/GetCurrentUserName', '/GetCurrentUserId'],
'passthrough' => ['/token','/clients'],
'authenticator' => $authenticator
]));

//Injectors
$container['logger'] = function($c){
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new Monolog\Handler\StreamHandler("..logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};
$container['testing'] = function($c){
    shell_exec('wall beginning of authentication');
};

$container['mapper'] = function($c){
    $mapper = new JsonMapper();
    return $mapper;
};
$container['encryption'] = function($c){
    $encryption = new Encryption();
    return $encryption;
};
$container['token'] = function($c)
{
    $token = new Token();
    return $token;
};
$container['debug'] = function($c){
    $debug = new debug();
    return $debug;
};
$container['login'] = function($c){
     return function ($request,$data)
    {
        // shell_exec("wall in login");  
        $authUtil = new AuthenticationUtility();
        $encryption = new Encryption();
        $token = new Token();
        $loggedIn = false;
        // gets client information
        $secret = $request->getHeader('Secret')[0];
        $email = $data["Username"];
        $password = $data["Password"];

        // need to check if it is still active

        
        $GLOBALS["container"]["debug"]->log("/token/login user ".$email,$password);



    ///region MAAAAAGEEEE
            // logs into Magento and gets customer START

        Mage::app('mygolo');
        Mage::getSingleton('core/session', array('name' => 'frontend'));
        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();
        $customer = Mage::getModel("customer/customer");
        $customer->website_id = $websiteId;
        $customer->setStore($store);
        //$customer->loadByEmail($email);
        $session = Mage::getSingleton('customer/session');//->setCustomerAsLoggedIn($customer);  

        if($session->login($email, $password)) {
            $wall = $session->getCustomer()->getId();
            $loggedIn = $wall;
        }
        else {
            throw new UnauthorizedException('Invalid login.');
        }        
        unset($authUtil);  
        return $loggedIn; 
        // logs into Magento and gets customer ENDdie
        };
 
 
 
    ///region     

};





///region Auth 
    ///<summary>gets clientID</summary>
    $app->post('/clients/addnewdevice', function (Request $request, Response $response) {
        try
        {
        
            $authUtil = new AuthenticationUtility();
            $data = $request->getParsedBody();
            $this->debug->log("/clients/addnewdevice deviceId".$data["DeviceId"]);
            $secret = $request->getHeader("Secret")[0];
            $current = $authUtil->GetAuthClientByDeviceId($data["DeviceId"]);
            
            if ($current != null) 
            {
                $auth = $this->encryption->encrypt($secret,$current->getId());
                $this->debug->log("/clients/addnewdevice exists cid".$clientId);
            }
            else
            {   
                unset($data);
                $authClient = $this->mapper->map(json_decode($request->getBody()), new AuthClient());
                shell_exec("wall wa".$authClient->getId());
                $auth = $this->token->register($authClient,$secret);
                
                $this->debug->log("/clients/addnewdevice new cid".$clientId);
            }
            return $response->withJson(array("client_id" => $auth), 200);
            unset($authUtil);
        } catch(Exception $e){
            unset($authUtil);        
            $this->debug->log("/clients/addnewdevice exception thrown 500 issued");
            return $response->withStatus(401);
        }
    });

    ///<summary>receives one of three calls: login, verify token, refresh old token</summary>
    ///<variables name=>$data=$response->getParsedBody()</variables>
    ///<variables name=>$header=$response->getHeader("key")[0]</variables>
    ///<values success=>200</values>
    ///<values error=>401</values>
    $app->post('/token', function (Request $request, Response $response) {
        try
        {

    #if LOG
            $this->debug->log("/token");
    #endif
            $authUtil = new AuthenticationUtility();
            $data = $request->getParsedBody();

            $clientId = $this->encryption->decrypt($secret,trim($data["client_id"]));
            $this->debug->log("/token cid ".$clientId);
            //checks to make sure client is in the table.
            $authClient = $authUtil->GetAuthClientById($clientId);
            if (!$authClient->getActive()) { 
    #if FATAL   
                $this->debug->log("/token throwing not active");
    #endif
                throw new UnauthorizedException('Client not active.');
            }

            // shell_exec("wall ".$grantType);
            if ($data['grant_type'] == "password") {
                $loginMethod = $this->login;
                $userId = $loginMethod($request,$data);
                
    #if LOG
                $this->debug->log("/token gt password uid ".$userId);
    #endif
                //gets data for both refresh and access token
                $accessToken = new AccessToken($userId,$clientId);

                // builds the return package for the server response.
                $promise = $this->token->issue($accessToken);
                unset($accessToken);

            } elseif ($data['grant_type'] == "token"){  
    #if LOG 
                $this->debug->log("/token gt token");
    #endif
                $bearer = new Bearer($request->getHeader("authorizeData")[0]);
    #if LOG 
                $this->debug->log("/token gt  bearer = ".$bearer->getAccessToken());
    #endif
                $promise = $this->token->validate($bearer->getAccessToken(),$request->getHeader('Secret')[0],$clientId);
            }elseif ($data['grant_type'] == "refreshToken"){        
    #if LOG
                $this->debug->log("/token gt refresh");
    #endif
                $refreshToken = $authUtil->GetRefreshTokenById(new RefreshToken($data['refresh_token']));
                $accessToken = new AccessToken($refreshToken->getUserId(),$refreshToken->getAuthClientId());
                //gets data for both refresh and access token
                // builds the return package for the server response.
                $promise = $this->token->issue($accessToken);
    #if LOG
                $this->debug->log("/token gt refresh accessToken cid".$accessToken->getClientId);
    #endif
                $authUtil->DeleteRefreshToken($refreshToken);
                unset($accessToken);
            }
            unset($authUtil);
            return $response->withJson($promise, 200);

        } catch(Exception $e){
            unset($authUtil);   
    #if FATAL     
            $this->debug->log("/token exception 401 thrown.\"".$e."\"");
    #endif
            return $response->withStatus(401);
        }
    });

///endregion
///reGION FUELTRACK

$app->post('/fueltrack/Add', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrack/Add start");
#endif
        $fuelTrackUtility = new FuelTrackUtility();
        $userId = intval($this->userId);
        $json = json_decode($request->getBody());
        $jsonOut = false;
        $fuelTrack = $this->mapper->map($json, new FuelTrack());
        $fuelTrack->setUserId($userId);

        if( $fuelTrackUtility->Add($fuelTrack)){
            $jsonOut =  $fuelTrackUtility->GetById($fuelTrack->getId());
#if LOG
            $this->debug->log("/fueltrack/Add jsonOut \"".$jsonOut->getUserId()."\"");
#endif
        }
        unset($fuelTrackUtility);
        return $response->withJson($jsonOut, 200, JSON_FORCE_OBJECT);
    } catch(Exception $e){
        unset($fuelTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrackweek/Add exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/fueltrack/Update', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrack/Update start");
#endif
        $fuelTrackUtility = new FuelTrackUtility();
        $userId = intval($this->userId);
        $data = $request->getParsedBody();
        $date = $data["Date"];
        $days = $data["Days"];
        for($i = 0 ; $i < count($days); $i++){
            $existingDay = $fuelTrackUtility->GetByUserDayWeekYear($userId, $i, intval($date["Week"]), intval($date["Year"]));
            if($days[$i]["ActualFitPoints"] !== null && $days[$i]["ActualFuelConsumed"] !== null){
                if($existingDay != null){
                    $existingDay->setActualFitPoints(intval($days[$i]["ActualFitPoints"]));
                    $existingDay->setActualFuelConsumed(intval($days[$i]["ActualFuelConsumed"]));
                    $existingDay->setBaseFuel(intval($days[i]["BaseFuel"]));
                    $fuelTrackUtility->Update($existingDay);

                    //debug if succeess
                } else {
                    $fuelTrack = new FuelTrack();
                    $fuelTrack->setUserId($userId);
                    $fuelTrack->setDay($i);
                    $fuelTrack->setWeek(intval($date["Week"]));
                    $fuelTrack->setMonth(intval($date["Month"]));
                    $fuelTrack->setYear(intval($date["Year"]));
                    $fuelTrack->setActualFitPoints(intval($days[$i]["ActualFitPoints"]));
                    $fuelTrack->setActualFuelConsumed(intval($days[$i]["ActualFuelConsumed"]));
                    $fuelTrack->setBaseFuel(intval($days[$i]["BaseFuel"]));
                    $fuelTrackUtility->Add($fuelTrack);
                    //deb ifd scs
                }
            } else if($days[$i]["ActualFitpoints"] && $days[$i]["ActualFuelConsumed"] == null){
                $fuelTrackUtility->Delete($existingDay);

                //debugifsccs
            }
        }
        unset($fuelTrackUtility);
        return $response->withJson(true, 200);
    } catch(Exception $e){
        unset($fuelTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrack/Update exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/fueltrack/GetByWeekAndYear', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrack/GetByWeekAndYear start");
#endif
        $fuelTrackUtility = new FuelTrackUtility();
        $userId = intval($this->userId);
        $data = $request->getParsedBody();
        $week = intval(filter_var($data["Week"], FILTER_SANITIZE_STRING));
        $year = intval(filter_var($data["Year"], FILTER_SANITIZE_STRING));
        $days = $fuelTrackUtility->GetByWeekAndYear($userId, $week, $year);

        $jsonOut = array("Days" => $days, "Week" => $week);
#if LOG
            $this->debug->log("/fueltrackweek/Update days = \"".$days."\" week = ",$week);
#endif
        unset($fuelTrackUtility);
        return $response->withJson($jsonOut, 200);
    } catch(Exception $e){
        unset($fuelTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrack/GetByWeekAndYear exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
///endregion
///region FuelTrackWeek
$app->post('/fueltrackweek/Add', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrackweek/Add start");
#endif
        $fuelTrackWeekUtility = new FuelTrackWeekUtility();
        $userId = intval($this->userId);
        $json = json_decode($request->getBody());
        $jsonOut = false;
        $fuelTrackWeek = $this->mapper->map($json, new FuelTrackWeek());
        $fuelTrackWeek->setUserId($userId);
#if LOG
            $this->debug->log("/fueltrackweek/Add jsonOut \"".$fuelTrackWeek->getUserId()."\"");
#endif
        if( $fuelTrackWeekUtility->Add($fuelTrackWeek)){
            $jsonOut =  $fuelTrackWeekUtility->GetById($fuelTrackWeek->getId());
        }
        unset($fuelTrackWeekUtility);
        return $response->withJson($jsonOut, 200, JSON_FORCE_OBJECT);
    } catch(Exception $e){
        unset($fuelTrackWeekUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrackweek/Add exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/fueltrackweek/Update', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrackweek/Update start");
#endif
        $fuelTrackWeekUtility = new FuelTrackWeekUtility();
        $userId = intval($this->userId);
        $json = json_decode($request->getBody());
        $fuelTrackWeek = $this->mapper->map($json, new FuelTrackWeek());
        $fuelTrackWeek->setUserId($userId);
        $fuelTrackWeekUtility->Update($fuelTrackWeek);
#if LOG
            $this->debug->log("/fueltrackweek/Update jsonOut \"".$fuelTrackWeek->getUserId()."\"");
#endif
        unset($fuelTrackWeekUtility);
        $response->getBody()->write('true');
        return $response->withStatus(200);
    } catch(Exception $e){
        unset($fuelTrackWeekUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrackweek/Update exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/fueltrackweek/GetByWeekAndYear', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrackweek/GetByWeekAndYear start");
#endif
        $fuelTrackWeekUtility = new FuelTrackWeekUtility();
        $userId = intval($this->userId);
        $json = $request->getParsedBody();
#if LOG
            $dbgstr1 = "/fueltrackweek/GetByWeekAndYear data = \"".$json["Week"]."\"";
            $this->debug->log($dbgstr1,$json["Year"]);
#endif
        $week = intval(filter_var($json["Week"], FILTER_SANITIZE_STRING));
        $year = intval(filter_var($json["Year"], FILTER_SANITIZE_STRING));
        $jsonOut = $fuelTrackWeekUtility->GetByWeekAndYear($userId, $week, $year);
#if LOG
            // $this->debug->log("/fueltrackweek/GetByWeekAndYear jsonOut \"".$jsonOut["userId"]."\"");
#endif

        unset($fuelTrackWeekUtility);
        return $response->withJson($jsonOut, 200, JSON_FORCE_OBJECT);
    } catch(Exception $e){
        unset($fuelTrackWeekUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrackweek/GetByWeekAndYear exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/fueltrackweek/ListPreviousFive', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/fueltrackweek/ListPreviousFive start");
#endif
        $fuelTrackWeekUtility = new FuelTrackWeekUtility();
        $userId = intval($this->userId);
        $json = $request->getParsedBody();
        $week = intval($json["Week"]);
        $year = intval($json["Year"]);
        $jsonOut = $fuelTrackWeekUtility->ListPreviousFive($userId, $week, $year);
#if LOG
            $this->debug->log("/fueltrackweek/ListPreviousFive year \"".$jsonOut[1]->getYear()."\" week ",$jsonOut[1]->getWeek());
#endif
        unset($fuelTrackWeekUtility);
        return $response->withJson($jsonOut, 200);
    } catch(Exception $e){
        unset($fuelTrackWeekUtility);
#if FATAL     
        $this->debug->fatalLog("/fueltrackweek/ListPreviousFive exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
///endregion///region ProgressTrack
$app->post('/progresstrack/Add', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/progresstrack/Add start");
#endif
        $progressTrackUtility = new ProgressTrackUtility();
        $userId = intval($this->userId);
        $json = json_decode($request->getBody());
        $jsonOut = false;
        $progressTrack = $this->mapper->map($json, new ProgressTrack());
        $progressTrack->setUserId($userId);
        if($progressTrackUtility->Add($progressTrack)){
            $jsonOut =  $progressTrackUtility->GetById($progressTrack->getId());
        }
#if LOG
            $this->debug->log("/progresstrack/Add ptid \"".$jsonOut->getId()."\"");
#endif
        unset($progressTrackUtility);
        return $response->withJson($jsonOut, 200);
    } catch(Exception $e){
        unset($progressTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/progresstrack/Add exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/progresstrack/Update', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/progresstrack/Update start");
#endif
        $progressTrackUtility = new ProgressTrackUtility();
        $userId = intval($this->userId);
        $json = json_decode($request->getBody());
        $progressTrack = $this->mapper->map($json, new ProgressTrack());
        $progressTrack->setUserId($userId);
        $jsonOut = $progressTrackUtility->Update($progressTrack);
#if LOG
            $this->debug->log("/progresstrack/Update jsonOut \"".$jsonOut."\"");
#endif
        unset($progressTrackUtility);
        return $response->withJson($progressTrack, 200);
    } catch(Exception $e){
        unset($progressTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/progresstrack/Update exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/progresstrack/Delete', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/progresstrack/Delete start");
#endif
        $progressTrackUtility = new ProgressTrackUtility();
        $userId = intval($this->userId);
        $json = json_decode($request->getBody(), true);
        $progressTrackId = intval($json["Id"]);
        $progressTrack = $progressTrackUtility->GetByID($progressTrackId);
        $progressTrack->setWeek(0);
        $success = $progressTrackUtility->Delete($progressTrack);
#if LOG
            $this->debug->log("/progresstrack/Delete jsonOut \"".$success."\"");
#endif
        unset($progressTrackUtility);
        return $response->withJson($progressTrack, 200);
    } catch(Exception $e){
        unset($progressTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/progresstrack/Delete exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withJson(array(pt => $progressTrack, herror => $e->getMessage()), 500);
    }
});
$app->post('/progresstrack/ListByUser', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/progresstrack/ListByUser start");
#endif
        $progressTrackUtility = new ProgressTrackUtility();
        $userId = intval($this->userId);
        $jsonOut = $progressTrackUtility->ListByUser($userId);
#if LOG
            $this->debug->log("/progresstrack/ListByUser uid \"".$jsonOut[0]->getId()."\"");
#endif
        unset($progressTrackUtility);
        return $response->withJson($jsonOut, 200);
    } catch(Exception $e){
        unset($progressTrackUtility);
#if FATAL     
        $this->debug->fatalLog("/progresstrack/ListByUser exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
///endregion///region MHA
$app->post('/GetMetabolicRateForCurrentUser', function(Request $request, Response $response){
    try {
#if LOG
            $this->debug->log("/GetMetabolicRateForCurrentUser start");
#endif
        $mhaUtility = new MHAUtility();
        $userId = intval($this->userId);
        $jsonOut = $mhaUtility->GetPersonalMetabolicRate($userId);
        if($jsonOut != 0){
#if LOG
            $this->debug->log("/GetMetabolicRateForCurrentUser pma = \"".$jsonOut."\"");
#endif
            unset($mhaUtility);
            return $response->withJson($jsonOut, 200);
        }
        unset($mhaUtility);
        return $response->withJson(false, 200);
    } catch (Exception $e){
        unset($mhaUtility);
#if FATAL     
        $this->debug->fatalLog("/GetMetabolicRateForCurrentUser exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});

$app->post('/GetPreviousFiveFullMHAs', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/GetPreviousFiveFullMHAs start");
#endif
        $mhaUtility = new MHAUtility();
        $userDetailsUtility = new UserDetailsUtility();
        $userId = intval($this->userId);
        // shell_exec('wall userid'.$this->$userId);
        $userDetails = $userDetailsUtility->GetById($userId);
        $jsonOut = $mhaUtility->GetLatestMHAResults($userId, $userDetails);
#if LOG
            $this->debug->log("/GetPreviousFiveFullMHAs jsonOut \"".$jsonOut["0"]->getLunchGuide()."\"");
#endif
        unset($mhaUtility);
        unset($userDetailsUtility);

        return $response->withJson($jsonOut, 200);
    } catch(Exception $e){
        unset($mhaUtility);
        unset($userDetailsUtility);
#if FATAL     
        $this->debug->fatalLog("/GetPreviousFiveFullMHAs exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/GetPreviousFiveMetabolicRate', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/GetPreviousFiveMetabolicRate start");
#endif
        $mhaUtility = new MHAUtility();
        $userId = intval($this->userId);
        $jsonOut = $mhaUtility->GetPreviousFiveMetabolicRates($userId);
#if LOG
            $this->debug->log("/GetPreviousFiveMetabolicRate value = \"".$jsonOut[0]->getPMR()."\"");
#endif
        unset($mhaUtility);
        return $response->withJson($jsonOut, 200);
    } catch(Exception $e){
        unset($mhaUtility);
#if FATAL     
        $this->debug->fatalLog("/GetPreviousFiveMetabolicRate exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/SaveMHAData', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/SaveMHAData start");
#endif
        $mhaUtility = new MHAUtility();
        $userDetailsUtility = new UserDetailsUtility();
        $userId = intval($this->userId);
        
        $json = json_decode($request->getBody());
#if LOG
                $this->debug->log("/SaveMHADATA mhadata \"".$json->{'MHAData'}->{"Id"}."\"");
#endif
        $mhaData = $this->mapper->map($json->{'MHAData'}, new MHAData());
        $userDetails = $userDetailsUtility->GetByID($userId);
        if($userDetails == null){
            $userDetails = $this->mapper->map($json->{'UserDetails'}, new UserDetails());
            if($userDetails != null){
                $userDetails->setUserId($userId);
                $userDetailsUtility->Add($userDetails);
#if LOG
                $this->debug->log("/SaveMHADATA jsonOut \"".$userDetails->getUserId()."\"");
#endif
            } else {
                throw new InvalidArgumentException('User details invalid.');
            }
        } else {
            $newDetails = $this->mapper->map($json->{'UserDetails'}, new UserDetails());
            $userDetails->setYear($newDetails->getYear());
            $userDetails->setHeightFT($newDetails->getHeightFT());
            $userDetails->setHeightIN($newDetails->getHeightIN());
            $userDetails->setSex($newDetails->getSex());
#if LOG
                $this->debug->log("/SaveMHADATA jsonOut \"".$newDetails["userId"]."\"");
#endif
            $userDetailsUtility->Update($userDetails);
        }
        if($mhaData != null){
            $mhaData->setDateCreated(new DateTime("now"));
            $mhaData->setUserId($userId);
            if(!$mhaUtility->AddMHAData($mhaData)){
#if FATAL     
        $this->debug->fatalLog("/SaveMHAData exception questionaire data inv. 500 thrown.\"".$e."\"");
#endif
                throw new InvalidArgumentException('MHA Questionaire Data invalid. Please check your answers and try again.');
            }
        }
        if(!$mhaUtility->AddMHAResults($mhaData, $userDetails)){
#if FATAL     
        $this->debug->fatalLog("/SaveMHAData exception unable to calculate. 500 thrown.\"".$e."\"");
#endif
            throw new InvalidArgumentException('Unable to calculate MHA Results. Please check your answers and try again.');
        }
        unset($mhaUtility);
        return $response->withJson(true, 200);
    } catch(Exception $e){
        unset($mhaUtility);
#if FATAL     
        $this->debug->fatalLog("/SaveMHAData exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/CheckUserDetails', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/CheckUserDetails start");
#endif
        $userDetailsUtility = new UserDetailsUtility();
        $userId = intval($this->userId);
        $result = false;
        $userDetails = $userDetailsUtility->GetByID($userId);
#if LOG
            $this->debug->log("/CheckUserDetails uid \"".$userDetails->getUserId()."\"");
#endif
        if($userDetails != null){
            $result = true;
        }
        return $response->withJson($result, 200);
        unset($mhaUtility);
        return $response;
    } catch(Exception $e){
        unset($mhaUtility);
#if FATAL     
        $this->debug->fatalLog("/CheckUserDetails exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
$app->post('/GetUserDetails', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/GetUserDetails start");
#endif
        $userDetailsUtility = new UserDetailsUtility();
        $userId = intval($this->userId);
        $result = false;
        $userDetails = $userDetailsUtility->GetByID($userId);
#if LOG
            $this->debug->log("/GetUserDetails uid = \"".$userDetails->getUserId()."\"");
#endif
        return $response->withJson($userDetails, 200);
        unset($mhaUtility);
        return $response;
    } catch(Exception $e){
        unset($mhaUtility);
#if FATAL     
        $this->debug->fatalLog("/GetUserDetails exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
});
///endregion
///region User
///<summary>Returns the user's ID.</summary>
$app->post('/GetCurrentUserName', function (Request $request, Response $response) {
    try{
#if LOG
            $this->debug->log("/GetCurrentUserName start");
#endif
        $customer = Mage::getModel('customer/customer')->load($this->userId);
#if LOG
            $this->debug->log("/GetCurrentUserName first name = \"".$customer->getFirstname()."\"");
#endif
        return $response->withJson($customer->getFirstname(), 200, JSON_FORCE_OBJECT);
    } catch(Exception $e) {
#if FATAL     
        $this->debug->fatalLog("/GetCurrentUserName exception. 500 thrown.\"".$e."\"");
#endif
        $response->getBody()->write($e->getMessage());
        return $response->withStatus(500);
    }
    
});

///<summary>Returns the user's ID.</summary>
$app->get('/GetCurrentUserId', function (Request $request, Response $response) {
#if LOG
            $this->debug->log("/GetCurrentUserId uid ".$this->userId." start");
#endif
    return $response->withJson(intval($this->userId), 200, JSON_FORCE_OBJECT);
});
#endif

$app->run();

?>