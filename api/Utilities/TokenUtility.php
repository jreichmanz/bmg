<?php
// api/Utilities/TokenUtility.php
namespace golo\ApiUtilities;
use golo\GoloContext as GoloContext;
use golo\AccessToken as AccessToken;
use golo\Encryption as Encryption;
use golo\Bearer as Bearer;
use golo\debug as debug;

use golo\models\AuthClient as AuthClient;
use golo\models\RefreshToken as RefreshToken;
use golo\utilities\AuthenticationUtility as AuthenticationUtility;

class Token
{
	function register(AuthClient $authClient,$secret)
	{
		$authUtil = new AuthenticationUtility();
		$ryption = new Encryption();
		$authClient->setSecret($secret);
		$GLOBALS["container"]["debug"]->log("tokenutil trace register. authclient id = \"".$authClient->getId()."\"");
		$authClient->setAllowedOrigin("*");
		$authClient->setRefreshTokenLifeTime(30);
		$authUtil->AddAuthClient($authClient);
		return $ryption->encrypt($secret,$authClient->getId());
	}
	function issue(AccessToken $accessToken)
	{
		$crypt = new Encryption();
		$tokenkit["access_token"] = $crypt->encrypt($secret,json_encode($accessToken));
		$tokenkit["refresh_token"] = $this->refresh($accessToken, $tokenkit["access_token"]);
		$GLOBALS["container"]["debug"]->log("tokenutil trace issue. refreshtoken id = \"".$tokenkit["refresh_token"]."\"");
		
		$tokenkit[".issued"] = $accessToken->getIssued();
		$tokenkit[".expires"] = $accessToken->getExpires();
		$tokenkit["access"] = true;
		unset($crypt);
		return $tokenkit;
	}

	function refresh(AccessToken $accessToken, $accessTokenEncrypted)
	{
		$authUtil = new AuthenticationUtility();
		$refreshToken = new RefreshToken();
        $refreshToken->setUserId($accessToken->getUserId());
        $refreshToken->setAuthClientId($accessToken->getClientId());
		$refreshToken->setIssuedUtc($accessToken->getIssued());
		$refreshToken->setExpiresUtc($accessToken->getExpires());
		$refreshToken->setProtectedTicket($accessTokenEncrypted);
		// saves refresh token.

		$authUtil->AddRefreshToken($refreshToken);
#if LOG
		$GLOBALS["container"]["debug"]->log("tokenutil trace refresh. refreshtoken id = \"".$refreshToken->getId()."\"");
#endif
		unset($authUtil);
		return $refreshToken->getId();
	}
	function validate($encryptedAccessToken,$secret,$clientId) {		
		$crypt = new Encryption();
		$accessToken = json_decode($crypt->decrypt($secret,$encryptedAccessToken));
		if ( (date("Y-m-d H:i:s") < $accessToken->{'expires'}) || ( $clientId != $accessToken->{"clientId"} )  ){
#if FATAL			
			$GLOBALS["container"]["debug"]->fatalLog("tokenutil exception- failed to validate. 401 thrown.\"".$accessToken."\"");
#endif	
			throw new UnauthorizedException("Invalid Token");
		}
		unset($crypt);	
#if LOG		
		$GLOBALS["container"]["debug"]->log("tokenutil validate trace. strcmp = \"".$waaaaa."\"");
#endif
#if LOG		
		$GLOBALS["container"]["debug"]->log("tokenutil validate trace. accessToken = \"".$accessToken->{"expires"}."\"");
#endif
		return $promise = array("access" => true);
	}
	function GetUserIdFromToken($encryptedAccessToken,$secret,$clientId) {		
		$crypt = new Encryption();	
		$accessToken = json_decode($crypt->decrypt($secret,$encryptedAccessToken));
#if LOG				
		$GLOBALS["container"]["debug"]->log("tokenutil GUIDFT trace. not accessToken = \"".$clientId."\" accesstoken ",$timeout);	
#endif
		if ( (date("Y-m-d H:i:s") < $accessToken->{'expires'}) || ( $clientId != $accessToken->{"clientId"} )  ){
#if FATAL
			$GLOBALS["container"]["debug"]->log("tokenutil exception- failed to retrieve userId. 401 thrown.\"".$accessToken."\"");
#endif		
			throw new UnauthorizedException("Invalid Token");
		}
		unset($crypt);	
		return $accessToken->{'userId'};
	}
}
?>
