<?php
// /Utilities/UserDetailsUtility.php
namespace golo\utilities;
use golo\models\RefreshToken as RefreshToken;
use golo\models\AuthClient as AuthClient;
use golo\GoloContext as GoloContext;

class AuthenticationUtility {
    //checked
    public function AddAuthClient(AuthClient $authClient){
        $context = new GoloContext();
        $context->db()->persist($authClient);
        $context->db()->flush();
        $context->db()->clear();
        return $authClient->getId() != null;
    }
    //checked
    public function UpdateAuthClient(AuthClient $authClient){
        $success = false;
        try{
            $context = new GoloContext();
            $context->db()->merge($authClient);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;
    }
    //checked
    public function DeleteAuthClient(AuthClient $authClient){
        $success = false;
        try{
            $context = new GoloContext();
            $delete = $context->db()->merge($authClient);
            $context->db()->remove($delete);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;
        
    }
    //checked
    public function GetAuthClientById($guid){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\AuthClient', 'pt')->where('pt.Id = ?1')->setParameter(1, $guid)->setMaxResults(1);
        $query = $qb->getQuery();
        $authClient = $query->getOneOrNullResult();
        
        shell_exec('wall got');
        $context->db()->clear();
        return $authClient;
    }
    public function GetAuthClientByDeviceId($deviceId){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\AuthClient', 'pt')->where('pt.DeviceId = ?1')->setParameter(1, $deviceId)->setMaxResults(1);
        $query = $qb->getQuery();
        $authClient = $query->getOneOrNullResult();
        shell_exec('wall got');
        $context->db()->clear();
        return $authClient;
    }
    public function AddRefreshToken(RefreshToken $RefreshToken)
    {
        $context = new GoloContext();
        $context->db()->persist($RefreshToken);
                    shell_exec('wall added');
        $context->db()->flush();
        $context->db()->clear();
        return $RefreshToken->getId() != null;
    }
    public function UpdateRefreshToken(RefreshToken $RefreshToken)
    {
        $success = false;
        try
        {
            $context= new GoloContext();
            $context->db()->merge($RefreshToken);
                        shell_exec('wall updated');
            $context->db()->flush();
            $success = true;
        } catch (Exception $e) {
            $success = false;
        }
        $context->db()->clear();
        return $success;
    }
    public function DeleteRefreshToken(RefreshToken $refreshToken)
    {
        $success = false;
        try{
            $context = new GoloContext();
            $delete = $context->db()->merge($refreshToken);
            $context->db()->remove($delete);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;

    }
    public function GetRefreshTokenById($guid)
    {
        try
        {
            $context = new GoloContext();
            $qb = $context->db()->createQueryBuilder();
            $qb->select('dt')->from('golo\models\RefreshToken','dt')->where('dt.Id = ?1')->setParameter(1, $guid)->setMaxResults(1);
            $query = $qb->getQuery();
            $RefreshToken = $query->getOneOrNullResult();
            shell_exec('wall got');
            $context->db()->clear();
            return $RefreshToken;
        } catch (Exception $e)
        {
            return $e;
        }
    }
    public function GetRefreshTokenByAuthClientId($guid)
    {
        try
        {
            $context = new GoloContext();
            $qb = $context->db()->createQueryBuilder();
            $qb->select('dt')->from('golo\models\RefreshToken','dt')->where('dt.AuthClientId = ?1')->setParameter(1, $guid)->setMaxResults(1);
            $query = $qb->getQuery();
            $RefreshToken = $query->getOneOrNullResult();
            shell_exec('wall got');
            $context->db()->clear();
            return $RefreshToken;
        } catch (Exception $e)
        {
            return $e;
        }
    }
}
?>