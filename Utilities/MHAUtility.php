<?php 
// /Utilities/MHAUtility.php
namespace golo\utilities;
use golo;
use golo\models\MHAData as MHAData;
use golo\models\MHAResults as MHAResults;
use golo\models\UserDetails as UserDetails;
use golo\models\wrappers\MHAFull as MHAFull;
use golo\models\wrappers\PersonalMetabolicRate as PersonalMetabolicRate;
use golo\models\EmotionalEatingData as EmotionalEatingData;
use golo\GoloContext as GoloContext;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

class MHAUtility {
    public function AddMHAData(MHAData $mhadata){
        $context = new GoloContext();
        $context->db()->persist($mhadata);
        $context->db()->flush();
        $context->db()->clear();
        return ($mhadata->getId() != null);
    }
        public function AddMHAResults(MHAData $data, UserDetails $userdetails){
        $context = new GoloContext();
        $results = new MHAResults();

        $age = date("Y") - $userdetails->getYear();
        $height = ($userdetails->getHeightFT() * 12) + $userdetails->getHeightIN();
        $bmi = round(($data->getWeight()) / ($height * $height) * (703), 2);
        $bmr = 666 + (6.23 * $data->getWeight()) + (12.7 * $height) - (6.8 * $age);
        $results->setMHADataId($data->getId());
        $results->setEstimatedGoloBMI(round(($data->getWeight()) / ($height * $height) * (703) / ($data->getFrame()), 2));
        $results->setIdealWeight(intval(($height * 2.54 - 100) * 2.2046));
        $IdealLower = (intval($results->getIdealWeight() * 0.95));
        $IdealUpper = (intval($results->getIdealWeight() * 1.05));
        $results->setIdealWeightRange("$IdealLower - $IdealUpper");
        $results->setIdealBMI(round(($results->getIdealWeight()) / ($height * $height) * (703) / ($data->getFrame()), 2));
        $results->setIdealWeightToLose($data->getWeight() - $results->getIdealWeight());
        $results->setPercentageToLose(round(floatval($results->getIdealWeightToLose() / $data->getWeight() * 100), 1));
        $results->setRealisticWeightLossGoalPerWeek(round(0.8 + 1 * ($results->getPercentageToLose() / 100), 2));
        $results->setWeightLossGoal($results->getIdealWeightToLose() *0.25);
        $results->setWeightLossGoalPercentage(round($results->getWeightLossGoal() / $data->getWeight() * 100, 2));
        $results->setWaistReductionGoal(round($data->getWaist() * ($results->getWeightLossGoalPercentage() / 100), 1));
        $results->setWaistAtWeightLossGoal(round($data->getWaist() - $results->getWaistReductionGoal(), 1) );
        $results->setProcessedFoodScore($data->getMealReplacementsPerWeek() + $data->getProcessedFoodsPerWeek() + $data->getSodasPerWeek());
        $results->setEmotionalEatingScore($data->getEmotionalEatingRating());
        //$results->setLifeStyleScore($age + intval($results->getEstimatedGoloBMI()) + $results->getProcessedFoodScore() + $results->getEmotionalEatingScore() + $data->getWaist() - $data->getActivity() - $data->getExcercisePerWeek() + $data->getCravingsRating() + $data->getSleepRating() + $data->getEnergyRating() + $data->getMoodRating() + $data->getStressRating());

        $results->setLifeStyleScore($bmi + $data->getMealReplacementsPerWeek() + $data->getProcessedFoodsPerWeek() + $data->getSodasPerWeek() + $data->getEmotionalEatingRating() + $data->getCravingsRating() + $data->getSleepRating() + $data->getEnergyRating() + $data->getMoodRating() + $data->getStressRating() - $data->getExcercisePerWeek());
        $genderValue = ($userdetails->getSex()) == 0 ? 390 : 360;
         if($age <= 20){
      $ageValue = 50;
  } else if($age <=30){
      $ageValue = 45;
  } else if ($age <=40){
    $ageValue = 40;
  } else if($age <= 50){
    $ageValue = 30;
  } else if($age <=60){
    $ageValue = 25;
  } else {
    $ageValue = 20;
  }
  if($results->getEstimatedGoloBMI() <=20){
    $metabolicRateValue = 40;
  } else if($results->getEstimatedGoloBMI()<=28){
    $metabolicRateValue = 35;
  }else if($results->getEstimatedGoloBMI()<=35){
      $metabolicRateValue = 25;
  } else {
    $metabolicRateValue = 20;
  }
      
        $results->setPersonalMetabolicRate($genderValue + $ageValue + $metabolicRateValue + $data->getActivity());
        $results->setBreakfastGuide(intval(round($results->getPersonalMetabolicRate() * 0.36),0));
        $results->setLunchGuide(intval(round($results->getPersonalMetabolicRate() * 0.36, 0)));
        $results->setDinnerGuide(intval(round($results->getPersonalMetabolicRate() * 0.28, 0)));
          if($age <=20){
             $ageValue = 60;
        } else if($age <=30){
            $ageValue = 55;
        } else if ($age <= 40){
            $ageValue = 50;
        } else if ($age <=50){
            $ageValue = 45;
        } else if ($age <=60){
            $ageValue = 40;
        } else if ($age <= 70){
            $ageValue = 35;
        } else {
            $ageValue = 30;
        }

        $activityValue = ($data->getActivity() == 35) ? 30 : $data->getActivity();
        $results->setFitPointsGoalPerWeek(80 + $ageValue + $activityValue + $data->getExcercisePerWeek());
        $results->setFitPointsPerDay(intval($results->getFitPointsGoalPerWeek() / 7));
        $results->setPMRWithFitPoints($results->getPersonalMetabolicRate() + $results->getFitPointsPerDay());
        $context->db()->persist($results);
        $context->db()->flush();
        $context->db()->clear();
        return ($results->getId() != null);
    }
    public function GetPersonalMetabolicRate($userId){
        $context = new GoloContext();
        $result = 0;
        $qb = $context->db()->createQueryBuilder();
        $qb->select('mha')->from('golo\Models\MHAData', 'mha')->where('mha.UserId = ?1')->orderBy('mha.DateCreated', 'DESC')->setParameter(1, $userId)->setMaxResults(1);
        $query = $qb->getQuery();
        $mha = $query->getOneOrNullResult();
        if($mha != null){
            $context->db()->clear();
            $qb = $context->db()->createQueryBuilder();
            $qb->select('mhaResult')->from('golo\Models\MHAResults', 'mhaResult')->where('mhaResult.MHADataId = ?1')->setParameter(1, $mha->getId())->setMaxResults(1);
            $query = $qb->getQuery();
            $mhaResult = $query->getOneOrNullResult();
            if($mhaResult != null){
                $result = $mhaResult->getPersonalMetabolicRate();
            }
        }
        $context->db()->clear();
        return $result; 
    }
    public function GetPreviousFiveMetabolicRates($userId){
        $context = new GoloContext();
#if LOG 
        //if ($context != null) {
        // $GLOBALS["container"]["debug"]->log("/MHAUTILITY GPFMBR context = \"".$context."\"");
        //}    
#endif   
        $qb = $context->db()->createQueryBuilder();
        $mhaQuery = $qb->select('mha')->from('golo\Models\MHAData', 'mha')->where('mha.UserId = ?1')->orderBy('mha.DateCreated', 'DESC')->setParameter(1, $userId)->getQuery();    

        $mhas = new ArrayCollection($mhaQuery->setMaxResults(5)->getResult());
        $mhaIds = array();
        foreach($mhas as $mha){
            $mhaIds[] = $mha->getId();
        }
#if LOG
        $GLOBALS["container"]["debug"]->log("/MHAUTILITY GPFMBR mhaId = \"".$mhaIds[0]."\"");
#endif   
        $context->db()->detach($qb); 
        $qb = $context->db()->createQueryBuilder();
        $query = $qb->select('mhaResult')->from('golo\models\MHAResults', 'mhaResult')->where($qb->expr()->in('mhaResult.MHADataId', $mhaIds))->getQuery();
        $results = $query->getResult();
        $pmrs = new ArrayCollection();
        foreach($mhas as $mha){
            $result = null;
            foreach($results as $item){
                if ($item->getMHADataId() == $mha->getId()){
                    $result = $item;
                }
            }
                if($result != null){
                    $pmr = new PersonalMetabolicRate();
                    $pmr->setPMR($result->getPersonalMetabolicRate());
                    $pmr->setDate($mha->getDateCreated());
#if LOG
                        $GLOBALS["container"]["debug"]->log("/MHAUTILITY GPFMBR pmrs = \"".$pmr->getPMR()."\"");
#endif  
                    $pmrs->add($pmr);
                } 
            }
         $context->db()->clear();
         return $pmrs->toArray();
    }
    public function GetLatestMHAResults($userId, UserDetails $userDetails){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $mhaQuery = $qb->select('mha')->from('golo\Models\MHAData', 'mha')->where('mha.UserId = ?1')->orderBy('mha.DateCreated', 'DESC')->setParameter(1, $userId)->getQuery();       
        $mhas = new ArrayCollection($mhaQuery->setMaxResults(5)->getResult());
        $mhaIds = array();
        foreach($mhas as $mha){
            $mhaIds[] = $mha->getId();
        }
        $context->db()->detach($qb); 
        $qb = $context->db()->createQueryBuilder();
        $query = $qb->select('mhaResult')->from('golo\Models\MHAResults', 'mhaResult')->where('mhaResult.MHADataId IN (:ids)')->setParameter('ids', $mhaIds)->getQuery();
        $results = new ArrayCollection($query->getResult());
        $fullMhas = new ArrayCollection();
        foreach($mhas as $mha){
            $result = null;
            foreach($results as $item){
                if ($item->getMHADataId() == $mha->getId()){
                    $result = $item;
                }
            }
            if($result != null){
            $fullResult = new MHAFull();
            $fullResult->setFrame($mha->getFrame());
            $fullResult->setDateCreated($mha->getDateCreated());
            $fullResult->setWeight($mha->getWeight());
            $fullResult->setWaist($mha->getWaist());
            $fullResult->setActivity($mha->getActivity());
            $fullResult->setExcercisePerWeek($mha->getExcercisePerWeek());
            $fullResult->setMealReplacementsPerWeek($mha->getMealReplacementsPerWeek());
            $fullResult->setProcessedFoodsPerWeek($mha->getProcessedFoodsPerWeek());
            $fullResult->setSodasPerWeek($mha->getSodasPerWeek());
            $fullResult->setEmotionalEatingRating($mha->getEmotionalEatingRating());
            $fullResult->setCravingsRating($mha->getCravingsRating());
            $fullResult->setSleepRating($mha->getSleepRating());
            $fullResult->setEnergyRating($mha->getEnergyRating());
            $fullResult->setMoodRating($mha->getMoodRating());
            $fullResult->setStressRating($mha->getStressRating());
            $fullResult->setProcessedFoodScore($result->getProcessedFoodScore());
            $fullResult->setEmotionalEatingScore($result->getEmotionalEatingScore());
            $fullResult->setLifeStyleScore($result->getLifeStyleScore());
            $fullResult->setEstimatedGoloBMI($result->getEstimatedGoloBMI());
            $fullResult->setIdealWeight($result->getIdealWeight());
            $fullResult->setIdealWeightRange($result->getIdealWeightRange());
            $fullResult->setIdealBMI($result->getIdealBMI());
            $fullResult->setIdealWeightToLose($result->getIdealWeightToLose());
            $fullResult->setPercentageToLose($result->getPercentageToLose());
            $fullResult->setWeightLossGoal($result->getWeightLossGoal());
            $fullResult->setWeightLossGoalPercentage($result->getWeightLossGoalPercentage());
            $fullResult->setRealisticWeightLossGoalPerWeek($result->getRealisticWeightLossGoalPerWeek());
            $fullResult->setWaistReductionGoal($result->getWaistReductionGoal());
            $fullResult->setWaistAtWeightLossGoal($result->getWaistAtWeightLossGoal());
            $fullResult->setPersonalMetabolicRate($result->getPersonalMetabolicRate());
            $fullResult->setBreakfastGuide($result->getBreakfastGuide());
            $fullResult->setLunchGuide($result->getLunchGuide());
            $fullResult->setDinnerGuide($result->getDinnerGuide());
            $fullResult->setFitPointsPerDay($result->getFitPointsPerDay());
            $fullResult->setFitPointsGoalPerWeek($result->getFitPointsGoalPerWeek());
            $fullResult->setPMRWithFitPoints($result->getPMRWithFitPoints());
            $fullMhas->add($fullResult);
            }
        }
        $context->db()->clear();
        return $fullMhas->toArray();
       //return $mhaIds;
    }
    // public function AddEmotionalEatingDataData(EmotionalEatingData $data){
    //     $context = new GoloContext();
    //     $context->db()->persist($data);
    //     $context->db()->flush();
    //     return $mhadata->getId() != null;
    // }
}
?>