<?php
// /Utilities/ProgressTrackUtility.php
namespace golo\utilities;
use golo\models\ProgressTrack as ProgressTrack;
use golo\GoloContext as GoloContext;

class ProgressTrackUtility {
    public function Add(ProgressTrack $progressTrack){
        $context = new GoloContext();
        $context->db()->persist($progressTrack);
        $context->db()->flush();
        $context->db()->clear();
        return $progressTrack->getId() != null;
    }
    public function Update(ProgressTrack $progressTrack){
        $success = false;
        try{
            $context = new GoloContext();
            $context->db()->merge($progressTrack);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;
    }
    public function Delete(ProgressTrack $progressTrack){
         $success = false;
        try{
            $context = new GoloContext();
 


            $attached = $context->db()->merge($progressTrack);   
            $context->db()->remove($attached);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;

    }
    public function GetById($progressTrackId){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\ProgressTrack', 'pt')->where('pt.Id = ?1')->setParameter(1, $progressTrackId)->setMaxResults(1);
        $query = $qb->getQuery();
        $progressTrack = $query->getOneOrNullResult();
        
        $context->db()->clear();
        return $progressTrack;
    }
    public function ListByUser($userId){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\ProgressTrack', 'pt')->where('pt.UserId = ?1')->setParameter(1, $userId);
        $query = $qb->getQuery();
        $progressTracks = $query->getResult();
        $context->db()->clear();
        return $progressTracks;
    }
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            $json[$key] = $value;
        }
        return $json; 
    }
}
?>
