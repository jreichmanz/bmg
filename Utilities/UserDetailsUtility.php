<?php
// /Utilities/UserDetailsUtility.php
namespace golo\utilities;
use golo\models\UserDetails as UserDetails;
use golo\GoloContext as GoloContext;

class UserDetailsUtility {
    public function Add(UserDetails $userDetails){
        $context = new GoloContext();
        $context->db()->persist($userDetails);
        $context->db()->flush();
        $context->db()->clear();
        return $userDetails->getId() != null;
    }
    public function Update(UserDetails $userDetails){
        $success = false;
        try{
            $context = new GoloContext();
            $context->db()->merge($userDetails);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;
    }
    public function Delete(UserDetails $userDetails){
         $success = false;
        try{
            $context = new GoloContext();
            $context->db()->remove($userDetails);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;

    }
    public function GetById($userId){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\UserDetails', 'pt')->where('pt.UserId = ?1')->setParameter(1, $userId)->setMaxResults(1);
        $query = $qb->getQuery();
        $userDetails = $query->getOneOrNullResult();
        $context->db()->clear();
        return $userDetails;
    }
    public function GetByMagentoId($magentoId){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\UserDetails', 'pt')->where('pt.Id = ?1')->setParameter(1, $magentoId)->setMaxResults(1);
        $query = $qb->getQuery();
        $userDetails = $query->getOneOrNullResult();
        $context->db()->clear();
        return $userDetails;
    }
   
}
?>