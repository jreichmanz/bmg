<?php
namespace golo\utilities;
use golo\models\CustomerDetails as CustomerDetails;
use golo\GoloContext as GoloContext;

class CustomerDetailsUtility {
    public function Add(CustomerDetails $customerDetails) {
        $context = new GoloContext();
        $context->conn()->persist($customerDetails);
        $context->conn()->flush();
        $context->conn()->clear();
        return $customerDetails->getId(); != null;
    }
        public function GetById($customerId) {
        $context = new GoloContext();
        $qb = $context->conn()->createQueryBuilder();
        $qb->select('eId')->from('golo\Models\CustomerDetails', 'eiD')->where('eiD.CustomerId = ?1')->setParameter(1, $customerId)->setMaxResults(1);
        $query = $qb->getQuery();
        $customerDetails = $query->getOneOrNullResult();
        $context->conn()->clear();
        return $customerDetails;
    }
}
}