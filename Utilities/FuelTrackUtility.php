<?php
// /Utilities/FuelTrackUtility.php
namespace golo\utilities;
use golo;
use golo\models\FuelTrack as FuelTrack;
use golo\GoloContext as GoloContext;

class FuelTrackUtility {
    public function Add(FuelTrack $fuelTrack){
        $context = new GoloContext();
        $context->db()->persist($fuelTrack);
        $context->db()->flush();
        $context->db()->clear();
        return $fuelTrack->getId() != null;
    }
    public function Update(FuelTrack $fuelTrack){
        $success = false;
        try{
            $context = new GoloContext();
            $context->db()->merge($fuelTrack);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;
    }
    public function Delete(FuelTrack $fuelTrack){
         $success = false;
        try{
            $context = new GoloContext();
            $context->db()->remove($fuelTrack);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;

    }
    public function GetById($Id){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrack', 'ft')->where('ft.Id = ?1')->setParameter(1, $Id);
        $query = $qb->getQuery();
        $fuelTracks = $query->getOneOrNullResult();
        $context->db()->clear();
        return $fuelTracks;
    }
    public function GetByWeek($userId, $week){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrack', 'ft')->where('ft.UserId = ?1')->andWhere('ft.Week = ?2')->orderBy('ft.Day', 'DESC')->setParameter(1, $userId)->setParameter(2, $week);
        $query = $qb->getQuery();
        $fuelTracks = $query->getResult();
        $context->db()->clear();
        return $fuelTracks;
    }
    public function GetByWeekAndYear($userId, $week, $year){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrack', 'ft')->where('ft.UserId = ?1')->andWhere('ft.Week = ?2')->andWhere('ft.Year = ?3')->orderBy('ft.Day', 'DESC')->setParameter(1, $userId)->setParameter(2, $week)->setParameter(3, $year);
        $query = $qb->getQuery();
        $fuelTracks = $query->getResult();
        $context->db()->clear();
        return $fuelTracks;
    }
    public function GetByUserDayWeekYear($userId, $day, $week, $year){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrack', 'ft')->where('ft.UserId = ?1')->andWhere('ft.Week = ?2')->andWhere('ft.Year = ?3')->andWhere('ft.Day = ?4')->orderBy('ft.Day', 'DESC')->setParameter(1, $userId)->setParameter(2, $week)->setParameter(3, $year)->setParameter(4, $day)->setMaxResults(1);
        $query = $qb->getQuery();
        $fuelTrackDay = $query->getOneOrNullResult();
        $context->db()->clear();
        return $fuelTrackDay;
    }
}
?>