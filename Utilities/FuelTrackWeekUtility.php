<?php
// /Utilities/FuelTrackWeekWeekUtility.php
namespace golo\utilities;

use golo\models\FuelTrackWeek as FuelTrackWeek;
use golo\GoloContext as GoloContext;

class FuelTrackWeekUtility {
    public function Add(FuelTrackWeek $FuelTrackWeekWeek){
        $context = new GoloContext();
        $context->db()->persist($FuelTrackWeekWeek);
        $context->db()->flush();
        $context->db()->clear();
        return $FuelTrackWeekWeek->getId() != null;
    }
    public function Update(FuelTrackWeek $FuelTrackWeekWeek){
        $success = false;
        try{
            $context = new GoloContext();
            $context->db()->merge($FuelTrackWeekWeek);
            $context->db()->flush();
            $success = true;
        } catch(Exception $e){
            $success = false;
        }
        $context->db()->clear();
        return $success;
    }
    public function GetByWeekAndYear($userId, $week, $year){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrackWeek', 'ft')->where('ft.UserId = ?1')->andWhere('ft.Week = ?2')->andWhere('ft.Year = ?3')->orderBy('ft.Year', 'DESC')->AddOrderBy('ft.Week', 'DESC')->setParameter(1, $userId)->setParameter(2, $week)->setParameter(3, $year);
        $query = $qb->getQuery();
        $fuelTrackWeeks = $query->setMaxResults(1)->getOneOrNullResult();
        $context->db()->clear();
        return $fuelTrackWeeks;
    }
    public function ListPreviousFive($userId, $week, $year){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrackWeek', 'ft')->where('ft.UserId = ?1')->andWhere('ft.Week < ?2')->andWhere('ft.Year = ?3')->orderBy('ft.Year', 'DESC')->AddOrderBy('ft.Week', 'DESC')->setParameter(1, $userId)->setParameter(2, $week)->setParameter(3, $year);
        $query = $qb->getQuery();
        $fuelTrackWeeks = $query->setMaxResults(5)->getResult();
        $context->db()->clear();
        return $fuelTrackWeeks;
    }
     public function GetById($fuelTrackWeekId){
        $context = new GoloContext();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('ft')->from('golo\Models\FuelTrackWeek', 'ft')->where('ft.Id = ?1')->setParameter(1, $fuelTrackWeekId)->setMaxResults(1);
        $query = $qb->getQuery();
        $fuelTrackWeek = $query->getOneOrNullResult();
        $context->db()->clear();
        return $fuelTrackWeek;
    }
  
}
?>