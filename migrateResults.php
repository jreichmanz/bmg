<?php 

// migrateResults.php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \golo\models;
use \golo\utilities;
//Models
use golo\models\MHAFull as MHAFull;
use golo\models\PersonalMetabolicRate as PersonalMetabolicRate;
use golo\models\EmotionalEatingData as EmotionalEatingData;
use golo\models\FuelTrack as FuelTrack;
use golo\models\FuelTrackWeek as FuelTrackWeek;
use golo\models\MHAData as MHAData;
use golo\models\MHAResults as MHResults;
use golo\models\ProgressTrack as ProgressTrack;
use golo\models\UserDetails as UserDetails;
//Utilities
use golo\utilities\FuelTrackUtility as FuelTrackUtility;
use golo\utilities\FuelTrackWeekUtility as FuelTrackWeekUtility;
use golo\utilities\MHAUtility as MHAUtility;
use golo\utilities\ProgressTrackUtility as ProgressTrackUtility;
use golo\utilities\UserDetailsUtility as UserDetailsUtility;

use golo\GoloContext as GoloContext;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;


require 'vendor/autoload.php';
require 'goloContext.php';

$updatedCount = 0;
$errorcount =0;
$startDate = DateTime::createFromFormat('Y-m-d', '1900-01-01'); 
$endDate = DateTime::createFromFormat('Y-m-d', '2017-03-02');
$context = new GoloContext();
$qb = $context->db()->createQueryBuilder();
$mhaQuery = $qb->select('mha')->from('golo\Models\MHAData', 'mha')->where('mha.DateCreated BETWEEN :start AND :end')->setParameter('start', $startDate->format('Y-m-d'))->setParameter('end', $endDate->format('Y-m-d'))->getQuery();       
$queery = $mhaQuery->getSql();
$mhas = $mhaQuery->getResult();
$context->db()->clear();
foreach($mhas as $mha){
  try{
        $qb = $context->db()->createQueryBuilder();
        $qb->select('pt')->from('golo\Models\UserDetails', 'pt')->where('pt.UserId = ?1')->setParameter(1, $mha->getUserId())->setMaxResults(1);
        $query = $qb->getQuery();
        $userdetails = $query->getOneOrNullResult();
        $context->db()->clear();
        $qb = $context->db()->createQueryBuilder();
        $qb->select('mhaResult')->from('golo\Models\MHAResults', 'mhaResult')->where('mhaResult.MHADataId = ?1')->setParameter(1, $mha->getId())->setMaxResults(1);
        $query = $qb->getQuery();
        $mhaResult = $query->getOneOrNullResult();
         $age = date("Y") - $userdetails->getYear();
        $height = ($userdetails->getHeightFT() * 12) + $userdetails->getHeightIN();
        $bmi = round(($mha->getWeight()) / ($height * $height) * (703), 2);
        $bmr = 666 + (6.23 * $mha->getWeight()) + (12.7 * $height) - (6.8 * $age);
        $mhaResult->setMHADataId($mha->getId());
        $mhaResult->setEstimatedGoloBMI(round(($mha->getWeight()) / ($height * $height) * (703) / ($mha->getFrame()), 2));
        $mhaResult->setIdealWeight(intval(($height * 2.54 - 100) * 2.2046));
        $IdealLower = (intval($mhaResult->getIdealWeight() * 0.95));
        $IdealUpper = (intval($mhaResult->getIdealWeight() * 1.05));
        $mhaResult->setIdealWeightRange("$IdealLower - $IdealUpper");
        $mhaResult->setIdealBMI(round(($mhaResult->getIdealWeight()) / ($height * $height) * (703) / ($mha->getFrame()), 2));
        $mhaResult->setIdealWeightToLose($mha->getWeight() - $mhaResult->getIdealWeight());
        $mhaResult->setPercentageToLose(round(floatval($mhaResult->getIdealWeightToLose() / $mha->getWeight() * 100), 1));
        $mhaResult->setRealisticWeightLossGoalPerWeek(round(0.8 + 1 * ($mhaResult->getPercentageToLose() / 100), 2));
        $mhaResult->setWeightLossGoal($mhaResult->getIdealWeightToLose() *0.25);
        $mhaResult->setWeightLossGoalPercentage(round($mhaResult->getWeightLossGoal() / $mha->getWeight() * 100, 2));
        $mhaResult->setWaistReductionGoal(round($mha->getWaist() * ($mhaResult->getWeightLossGoalPercentage() / 100), 1));
        $mhaResult->setWaistAtWeightLossGoal(round($mha->getWaist() - $mhaResult->getWaistReductionGoal(), 1) );
        $mhaResult->setProcessedFoodScore($mha->getMealReplacementsPerWeek() + $mha->getProcessedFoodsPerWeek() + $mha->getSodasPerWeek());
        $mhaResult->setEmotionalEatingScore($mha->getEmotionalEatingRating());
        $mhaResult->setLifeStyleScore($bmi + $mha->getMealReplacementsPerWeek() + $mha->getProcessedFoodsPerWeek() + $mha->getSodasPerWeek() + $mha->getEmotionalEatingRating() + $mha->getCravingsRating() + $mha->getSleepRating() + $mha->getEnergyRating() + $mha->getMoodRating() + $mha->getStressRating() - $mha->getExcercisePerWeek());
        $genderValue = ($userdetails->getSex() == 0) ? 390 : 360;
         if($age <= 20){
            $ageValue = 50;
        } else if($age <=30){
            $ageValue = 45;
        } else if ($age <=40){
            $ageValue = 40;
        } else if($age <= 50){
            $ageValue = 30;
        } else if($age <=60){
            $ageValue = 25;
        } else {
            $ageValue = 20;
        }
        if($mhaResult->getEstimatedGoloBMI() <=20){
            $metabolicRateValue = 40;
        } else if($mhaResult->getEstimatedGoloBMI()<=28){
            $metabolicRateValue = 35;
        }else if($mhaResult->getEstimatedGoloBMI()<=35){
            $metabolicRateValue = 25;
        } else {
            $metabolicRateValue = 20;
        }
      
        $mhaResult->setPersonalMetabolicRate($genderValue + $ageValue + $metabolicRateValue + $mha->getActivity());
        $mhaResult->setBreakfastGuide(intval(round($mhaResult->getPersonalMetabolicRate() * 0.36),0));
        $mhaResult->setLunchGuide(intval(round($mhaResult->getPersonalMetabolicRate() * 0.36, 0)));
        $mhaResult->setDinnerGuide(intval(round($mhaResult->getPersonalMetabolicRate() * 0.28, 0)));
        if($age <=20){
             $ageValue = 60;
        } else if($age <=30){
            $ageValue = 55;
        } else if ($age <= 40){
            $ageValue = 50;
        } else if ($age <=50){
            $ageValue = 45;
        } else if ($age <=60){
            $ageValue = 40;
        } else if ($age <= 70){
            $ageValue = 35;
        } else {
            $ageValue = 30;
        }
        
        $activityValue = ($mha->getActivity() == 35) ? 30 : $mha->getActivity();
        $mhaResult->setFitPointsGoalPerWeek(80 + $ageValue + $activityValue + $mha->getExcercisePerWeek());
        $mhaResult->setFitPointsPerDay(intval($mhaResult->getFitPointsGoalPerWeek() / 7));
        $mhaResult->setPMRWithFitPoints($mhaResult->getPersonalMetabolicRate() + $mhaResult->getFitPointsPerDay());
        $context->db()->merge($mhaResult);
        $context->db()->flush();
        $context->db()->clear();
        $updatedCount++;
    
  } catch(Exception $e) {
      var_dump($e->getMessage());
      var_dump($mha->getId());
      $errorcount++;
  }
}

 ?> 

 <h1>Updated <?php echo($updatedCount) ?> Results</h1>
 <p><?php echo($errorcount) ?></p>
 