<?php
if(@$_SERVER['HTTPS'] == 'on'){
	header('Location: http://test2.mygolo.com/tracking');
	exit();
}
use League\OAuth1\Client\Server\Magento as Magento;
use golo\models\User as User;
use golo\utilities\UserUtility as UserUtility;


require_once 'goloContext.php';
require_once 'vendor/autoload.php';
require_once '../app/Mage.php';

/* Mage::app('mygolo');
Mage::getSingleton('core/session', array('name' => 'frontend'));
if(!Mage::getSingleton('customer/session')->isLoggedIn()){
    Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
    header('Location: /customer/account/login/');
	// echo 'aaaaaaa';
}
// die; */
?>
<!DOCTYPE html>
<html ng-app="GoloApp">

<head>
    <title>GOLO for Life | testing</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="css/bootstrap-slider.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/swipebox.min.css" />
    <link rel="stylesheet" href="css/ng-custom.css" />
    <link rel="stylesheet" href="css/site-style.css" />
    <link rel="stylesheet" href="css/zebra_datepicker.css" />
        <link rel="stylesheet" href="/js/smartwave/ajaxcart/ajaxaddto.css?v=1.0.1" />

	        <link rel="stylesheet" href="/skin/frontend/smartwave/porto/css/local.css" ng-if="!$root.results" />
	        <link rel="stylesheet" type="text/css" href="https://www.mygolo.com/skin/frontend/smartwave/porto/css/responsive.css?v=1.0.2" media="all">
                <link rel="stylesheet" href="/skin/frontend/smartwave/porto/icons/css/porto.css" />
            <script src="https://test2.mygolo.com/tracking/Scripts/jquery-2.0.3.min.js"></script>
           
            
</head>

<body ng-cloak class="boxed" style="position:relative;min-height:100%;top:0px" >
    <div class="wrapper">
    	<div class="page">
<div class="header-container type5">

    <div class="header container">
                <a href="https://www.mygolo.com/index.php/" title="GOLO" class="logo"><strong>GOLO</strong><img src="https://www.mygolo.com/skin/frontend/smartwave/porto/images/mygolo_logo.png" alt="GOLO"></a>
                <div class="cart-area">
            <div class="menu-area">
                <div class="links-area">
                    <div class="custom-block"></div>                    <div class="top-links-icon"><a href="javascript:void(0)">links</a></div>
                    <ul class="links">
                        <li class="first"><a href="https://www.mygolo.com/index.php/customer/account/" title="My Account">My Account</a></li>
                                <li class=" last"><a href="https://www.mygolo.com/index.php/customer/account/logout/" title="Log Out">Log Out</a></li>
            </ul>
                </div>
               

             
				<div class="menu-icon"><a href="javascript:void(0)" title="Menu"><i class="fa fa-bars"></i></a></div>
            </div>
             </div>
        
            </div>
</div>
<div class="header-bottom container">
	<ul class="links2">
  	<li class="first"><a href="/pages/downloads.html" title="Downloads">Downloads</a></li>
  	<li class=""><a href="/tracking/" title="myTracking">myTracking</a></li>
  	<li class="linkstyle2 last"><a href="/products.html" title="Shop Golo">Shop Golo</a></li>
</ul>	
</div>
<div class="top-container" >
	<div class="category-banner" ui-view="header" style="padding:15px"></div>
</div>

	 <div class="main-container col2-left-layout">
	 	<div class="main container">
	 		<div class="row">
	 			<div class="col-main col-sm-9 f-right">
	 			 <div id="main_content" ui-view="content" ></div>
    

	 			</div>
	 			<div class="col-left sidebar f-left col-sm-3">
	 			<div class="mj">
    <div class="mj-title"><strong><span><a href="/pages/golo-rescue-plan.html" title="Golo Rescue Plan">GOLO Rescue Plan</a></span></strong></div>
    <div class="sj-title"><strong><span><a href="/pages/golo-rescue-plan/7-day-kickstart-plan.html" title="7 Day Kickstart Plan">- 7 Day Kickstart Plan</a></span></strong></div>
    <div class="sj-title"><strong><span><a href="/pages/golo-rescue-plan/golo-for-life.html" title="Golo For Life">- Golo For Life</a></span></strong></div>
    <div class="sj-title"><strong><span><a href="/pages/golo-rescue-plan/metabolic-fuel-matrix.html" title="Metabolic Fuel Matix">- Metabolic Fuel Matix</a></span></strong></div>
    <div class="sj-title"><strong><span><a href="/pages/golo-rescue-plan/smart-card.html" title="Smart Card">- Smart Card</a></span></strong></div>
	<div class="sj-title"><strong><span><a href="/tracking/" title="MyTracking">- myTracking</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="/pages/golo-graduates.html" title="Golo Graduates">GOLO Graduates</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="h/pages/health-news.html" title="Health News">Health News</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="/pages/tips-and-recipes.html" title="Tips &amp; Recipes">Tips &amp; Recipes</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="/pages/golo-fit.html" title="Golo Fit">Golo Fit</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="/pages/support.html" title="Support">Support</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="/pages/downloads.html" title="Downloads">Downloads</a></span></strong></div>
    <div class="mj-title"><strong><span><a href="/products.html" title="Shop Golo">Shop Golo</a></span></strong></div>
</div><br/><br/><div class="aj">
    <div class="aj-title"><strong><span><a href="/customer/account" title="My Account">My Account</a></span></strong></div>
</div>
	 			</div>
	 		</div>
	 	</div>
	 <div>
	 
    	    	</div>
    	    	<div ui-view="progtrack"></div>
    </div>
    
   
    
   <div class="footer-container ">
    <div class="footer">
            <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    <div class="footermygolo"><br><br><img alt="" src="https://www.mygolo.com/media/wysiwyg/mygolo/copyright.png"><br><br><span style="font-size:10px; text-align:right; color: #ffffff;">© 2014-2016 GOLO, LLC</span></div>                    </div>
                </div>
            </div>
        </div>
        </div>
</div>
</div></div>
 <div ui-view="fullpage"></div>
    <script src="https://test2.mygolo.com/tracking/Scripts/zebra_datepicker.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/bootstrap.min.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/bootstrap-slider.min.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/jquery.swipebox.min.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/jspdf.min.js"></script>
	<script src="https://test2.mygolo.com/tracking/Scripts/angular-1.5.8.min.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/angular-ui-router.min.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/AngularValidator.js"></script>
	<script src="https://test2.mygolo.com/tracking/Scripts/ng-file-upload-shim.js"></script>
	<script src="https://test2.mygolo.com/tracking/Scripts/ng-file-upload.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/moment.min.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/GoloApp.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/GoloService.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/AnalysisCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/layout/sidebarCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/layout/TopHeaderCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/layout/pageHeaderCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/layout/PdfCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/layout/MetabolicRateCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/content/AccountCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/FuelTrackingCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/ProgressTrackingCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/DataSummaryCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/mha-tracking.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/MHAQuestionnaireCtrl.js"></script>
    <script src="https://test2.mygolo.com/tracking/Scripts/MHAResultsPrintCtrl.js"></script>

</body>

</html>

