<?php

namespace golo;
//AccessToken.php
class AccessToken implements \JsonSerializable {
    private $userId;
    private $clientId;
    private $issued;
    private $expires;
    function __construct($userId, $clientId){
        $this->userId = $userId; $this->clientId = $clientId; $this->issued = new \DateTime("now"); ; $this->expires = new \DateTime("+30 minutes");
    }
    function getClientId(){
        return $this->clientId;
    }
        function getUserId(){
        return $this->userId;
    }
        function getIssued(){
        return $this->issued;
    }
        function getExpires(){
        return $this->expires;
    }
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('Y-m-d H:i:s');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json;
    }  
}
?>