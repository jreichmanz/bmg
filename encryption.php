<?php
//Encryption.php
namespace golo;
class Encryption{
    private $privKey;
    private $pubKey;
    function __construct(){
        $this->privKey = "-----BEGIN RSA PRIVATE KEY-----
        MIIEogIBAAKCAQEAzYiBryIFyBI5YacT7/CrcyIZCH2CJwRrbFn1Br0DXmllYsZn
        xZxTVVwfhg7PxZOHRIvLHfDGTJitrnLf7DbsPLAriIqup9WRXnkpWU50dEUXw5ve
        x2ibEDKf2UQg9nu18G1QpvIaCh487EkSie5ftL/95kRZMVrs1a2iIg59TFGy1mLh
        D+yBCe1mEBfWNzFyJQcUyp6h9ZeexuDY+JjCkmuHJybp5980S9b/PM7CFaIJOBZk
        /2NqgDTnOz8NEGbZnvyJckFZuB9ijRLaatImjLo8dHafscuhb2lI1bTBCL8xepv8
        4VPz3eJ/BlWPmk0bzQDOm0COdH5MHVLg5XWSWQIDAQABAoIBAD5iVJqHxSF5Bpmx
        UMffUOrdSqPdFIgrOz/j/g9Rh5g8fV2BwzWjwVeDYr4Z+M6LfN89XyVzU30kSnNM
        wtK7yPCSuGP1VBX1KO538Cj/OzGfVxs0UGZOReKguwwDNc4atpqHU5eYnHa9vtgp
        Rcs/4yTFsZcW38uvQXuJ/FBB/Z4QEswewwMWz1BIvhApQDT7wR6hS455VXAfr81u
        uC/3JkJjop0QbIiAqFwgR1Rns12EPP6hfNHr/gIM3uXfCB7/Su1/TxKNPOTOG61I
        YVM8IJonDmu7n2YhWLloD9a6PfxaCEQxTOE4Ar+l78bmhtiNgUjoXrWCcgByondk
        eIOc9gECgYEA72OxC3vzycGgXoi1Ys+5zqOSTpgUgDOSUFS58zQrZkMdg2mczMaZ
        k7vj43CfFRDgNZ8LLVkXCdaNbG54ANpBSANVh8VB9UTDmRjt+0BiERnL0kk43UHr
        uKUNW9kDqHy4h6fWD3XpcuqCit3GmQif6BS33aUYsSAR2TblxSeDt7ECgYEA28ts
        MhySFuOBnBUp7FlCia89j8nFOs8U57uhSpP+tK18LW21pMM01ZMU4pjFr4Z8m5q1
        7OlCwIo3Map35oDme8Vsd//stogd9KbMMW66/IEXlvhn+44VJv3uDkHnI3p4yDfb
        EjGlckiqPm4tsuMpZQ5i5B6NE5oQrXS5mxjwVykCgYBW5RwF7CdxC+zXg847oOV0
        YaGhD8kZRB8PM8IPytcaFrCm2/ZDyq9ClghBrcbBEdAhBqs6+cTIGG5bdrnurUrX
        BgbM8shfbGeHsinoctVmGbQHj/apMd2Lt7XiqiKOzT76Vuz3zbhAs2lRWPFW6W+K
        AcNAHEabPOXdr/S1nQNN8QKBgGMK+6ppLXua8WDAOLAEWPNqQDd3GKUzg5wY9MmC
        fxgzxO1DrsKcJEbJjHO/m/VIiQYlZS1ov2e74LtEsdrw4uuLPyGj9asbE0SXlRQB
        bcky8spZDaJRmD6F1qweDmXyXlL6uIEuVft1uyF+zKD7tD+RiAZ7rd7TYgiHvMuj
        JUphAoGABa/ME3/e4SkgSCNhsTJDo1Lyld4v+XbQzXQ4BxvwosjvWEZjFLVSeUbr
        XqipouCECXJ4rwUHeWNFBSBZ4UwhseqCqgl/P+7Fh0jNI8rG0y5I9EwaaU1Qa6Mi
        QOVAZ0WM6S9AvdBcFueXOv1lIFdRiYiQMXmp0PVsBkobS2a/z0M=
        -----END RSA PRIVATE KEY-----";
        $this->pubKey = "-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzYiBryIFyBI5YacT7/Cr
        cyIZCH2CJwRrbFn1Br0DXmllYsZnxZxTVVwfhg7PxZOHRIvLHfDGTJitrnLf7Dbs
        PLAriIqup9WRXnkpWU50dEUXw5vex2ibEDKf2UQg9nu18G1QpvIaCh487EkSie5f
        tL/95kRZMVrs1a2iIg59TFGy1mLhD+yBCe1mEBfWNzFyJQcUyp6h9ZeexuDY+JjC
        kmuHJybp5980S9b/PM7CFaIJOBZk/2NqgDTnOz8NEGbZnvyJckFZuB9ijRLaatIm
        jLo8dHafscuhb2lI1bTBCL8xepv84VPz3eJ/BlWPmk0bzQDOm0COdH5MHVLg5XWS
        WQIDAQAB
        -----END PUBLIC KEY-----";
    }
    function encrypt($secret, $data){
        $stageOne = openssl_encrypt($data, 'aes-256-ofb', $this->pubKey.$secret);
        $stageTwo = openssl_encrypt($stageOne, 'aes-256-ofb', $this->pubKey);
        $encrypted = base64_encode($stageTwo);
        return $encrypted;
    }
    function decrypt($secret, $data){
        $stageOne = base64_decode($data);
        $stageTwo = openssl_decrypt( $stageOne, 'aes-256-ofb', $this->pubKey);
        $decrypted = openssl_decrypt($stageTwo, 'aes-256-ofb', $this->pubKey.$secret);
        return $decrypted;
    }
} ?>