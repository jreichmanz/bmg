<?php
// cli-config.php
require_once "goloContext.php";
$context = new \golo\GoloContext();

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($context->db());
?>