<?php
namespace golo;
//Bearer.php
class Bearer implements \JsonSerializable {
    private $access_token;
    // private $token_type;
    // private $as_client_id;
    // private $issued;
    // private $expires;
    function __construct($token){
        // $values = json_decode($token, true);
        // split on space to get access token
        $this->access_token = explode("Bearer ", $token)[1];
        // $this->token_type = $values["token_type"];
        // $this->as_client_id = $values["as:client_id"];
        // $this->issued = $values[".issued"];
        // $this->expires = $values[".expires"];
    }
    function getAccessToken(){
        return $this->access_token;
    }
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y H:i:s');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json;
    }
}
?>