<?php
// /Models/FuelTrack.php
namespace golo\models;
/**
 * @Entity @Table(name="FuelTrack")
 **/
class FuelTrack implements \JsonSerializable { 
    /**
    *@var int 
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int 
    *@Column(type="integer", nullable=false) 
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Day;
    public function getDay(){return $this->Day;}
    public function setDay($value){$this->Day = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Week;
    public function getWeek(){return $this->Week;}
    public function setWeek($value){$this->Week = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Month;
    public function getMonth(){return $this->Month;}
    public function setMonth($value){$this->Month = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Year;
    public function getYear(){return $this->Year;}
    public function setYear($value){$this->Year = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $ActualFitPoints;
    public function getActualFitPoints(){return $this->ActualFitPoints;}
    public function setActualFitPoints($value){$this->ActualFitPoints = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $ActualFuelConsumed;
    public function getActualFuelConsumed(){return $this->ActualFuelConsumed;}
    public function setActualFuelConsumed($value){$this->ActualFuelConsumed = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $BaseFuel;
    public function getBaseFuel(){return $this->BaseFuel;}
    public function setBaseFuel($value){$this->BaseFuel = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>