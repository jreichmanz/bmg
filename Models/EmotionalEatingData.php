<?php
// /Models/EmotionalEatingData.php
namespace golo\models;
//need to add table annotation when ready to use
class EmotionalEatingData implements \JsonSerializable {
    /**
    *@var int 
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int 
    *@Column(type="integer", nullable=false) 
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E01;
    public function getE01(){return $this->E01;}
    public function setE01($value){$this->E01 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S13;
    public function getS13(){return $this->S13;}
    public function setS13($value){$this->S13 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C21;
    public function getC21(){return $this->C21;}
    public function setC21($value){$this->C21 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I37;
    public function getI37(){return $this->I37;}
    public function setI37($value){$this->I37 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E07;
    public function getE07(){return $this->E07;}
    public function setE07($value){$this->E07 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I35;
    public function getI35(){return $this->I35;}
    public function setI35($value){$this->I35 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C27;
    public function getC27(){return $this->C27;}
    public function setC27($value){$this->C27 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S15;
    public function getS15(){return $this->S15;}
    public function setS15($value){$this->S15 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E04;
    public function getE04(){return $this->E04;}
    public function setE04($value){$this->E04 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C25;
    public function getC25(){return $this->C25;}
    public function setC25($value){$this->C25 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I38;
    public function getI38(){return $this->I38;}
    public function setI38($value){$this->I38 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E05;
    public function getE05(){return $this->E05;}
    public function setE05($value){$this->E05 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S11;
    public function getS11(){return $this->S11;}
    public function setS11($value){$this->S11 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C22;
    public function getC22(){return $this->C22;}
    public function setC22($value){$this->C22 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I32;
    public function getI32(){return $this->I32;}
    public function setI32($value){$this->I32 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S18;
    public function getS18(){return $this->S18;}
    public function setS18($value){$this->S18 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E08;
    public function getE08(){return $this->E08;}
    public function setE08($value){$this->E08 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C24;
    public function getC24(){return $this->C24;}
    public function setC24($value){$this->C24 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I36;
    public function getI36(){return $this->I36;}
    public function setI36($value){$this->I36 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C29;
    public function getC29(){return $this->C29;}
    public function setC29($value){$this->C29 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E03;
    public function getE03(){return $this->E03;}
    public function setE03($value){$this->E03 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S12;
    public function getS12(){return $this->S12;}
    public function setS12($value){$this->S12 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I34;
    public function getI34(){return $this->I34;}
    public function setI34($value){$this->I34 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E02;
    public function getE02(){return $this->E02;}
    public function setE02($value){$this->E02 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S16;
    public function getS16(){return $this->S16;}
    public function setS16($value){$this->S16 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C23;
    public function getC23(){return $this->C23;}
    public function setC23($value){$this->C23 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I31;
    public function getI31(){return $this->I31;}
    public function setI31($value){$this->I31 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C26;
    public function getC26(){return $this->C26;}
    public function setC26($value){$this->C26 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E09;
    public function getE09(){return $this->E09;}
    public function setE09($value){$this->E09 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S14;
    public function getS14(){return $this->S14;}
    public function setS14($value){$this->S14 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I40;
    public function getI40(){return $this->I40;}
    public function setI40($value){$this->I40 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S17;
    public function getS17(){return $this->S17;}
    public function setS17($value){$this->S17 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C30;
    public function getC30(){return $this->C30;}
    public function setC30($value){$this->C30 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E06;
    public function getE06(){return $this->E06;}
    public function setE06($value){$this->E06 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S19;
    public function getS19(){return $this->S19;}
    public function setS19($value){$this->S19 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I33;
    public function getI33(){return $this->I33;}
    public function setI33($value){$this->I33 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $C28;
    public function getC28(){return $this->C28;}
    public function setC28($value){$this->C28 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $S20;
    public function getS20(){return $this->S20;}
    public function setS20($value){$this->S20 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $E10;
    public function getE10(){return $this->E10;}
    public function setE10($value){$this->E10 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $I39;
    public function getI39(){return $this->I39;}
    public function setI39($value){$this->I39 = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ETotal;
    public function getETotal(){return $this->ETotal;}
    public function setETotal($value){$this->ETotal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $STotal;
    public function getSTotal(){return $this->STotal;}
    public function setSTotal($value){$this->STotal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $CTotal;
    public function getCTotal(){return $this->CTotal;}
    public function setCTotal($value){$this->CTotal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ITotal;
    public function getITotal(){return $this->ITotal;}
    public function setITotal($value){$this->ITotal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Total;
    public function getTotal(){return $this->Total;}
    public function setTotal($value){$this->Total = $value;}

    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>