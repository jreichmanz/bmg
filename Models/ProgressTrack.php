<?php
// /Models/ProgressTrack.php
namespace golo\models;
/**
 * @Entity @Table(name="ProgressTrack")
 **/
class ProgressTrack implements \JsonSerializable {
     /**
    *@var int
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int 
    *@Column(type="integer", nullable=false) 
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Week;
    public function getWeek(){return $this->Week;}
    public function setWeek($value){$this->Week = $value;}
    /**
    *@var datetime
    *@Column(type="datetime", nullable=true)
    **/
    protected $Date;
    public function getDate(){return $this->Date;}
    public function setDate($value){$this->Date = new \DateTime($value);}
    /**
    *@var int
    *@Column(type="integer", nullable=true) 
    **/
    protected $Weight;
    public function getWeight(){return $this->Weight;}
    public function setWeight($value){$this->Weight = $value;}
    /**
    *@var int
    *@Column(type="integer", nullable=true) 
    **/
    protected $Waist;
    public function getWaist(){return $this->Waist;}
    public function setWaist($value){$this->Waist = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Sleep;
    public function getSleep(){return $this->Sleep;}
    public function setSleep($value){$this->Sleep = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Energy;
    public function getEnergy(){return $this->Energy;}
    public function setEnergy($value){$this->Energy = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Mood;
    public function getMood(){return $this->Mood;}
    public function setMood($value){$this->Mood = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Stress;
    public function getStress(){return $this->Stress;}
    public function setStress($value){$this->Stress = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>