<?php
// /Models/RegisterModels.php
require "AuthClient.php";
require "EmotionalEatingData.php";
require "FuelTrack.php";
require "FuelTrackWeek.php";
require "MHAData.php";
require "MHAResults.php";
require "ProgressTrack.php";
require "RefreshToken.php";
require "UserDetails.php";
require "Wrappers/MHAFull.php";
require "Wrappers/PersonalMetabolicRate.php";
?>