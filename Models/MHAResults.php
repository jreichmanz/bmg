<?php
// /Models/MHAResults.php
namespace golo\models;
/**
 * @Entity @Table(name="MHAResults")
 **/
class MHAResults implements \JsonSerializable {
    
    /**
    *@var int 
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int 
    *@Column(type="integer", nullable=false) 
    **/
    protected $MHADataId;
    public function getMHADataId(){return $this->MHADataId;}
    public function setMHADataId($value){$this->MHADataId = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ProcessedFoodScore;
    public function getProcessedFoodScore(){return $this->ProcessedFoodScore;}
    public function setProcessedFoodScore($value){$this->ProcessedFoodScore = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $EmotionalEatingScore;
    public function getEmotionalEatingScore(){return $this->EmotionalEatingScore;}
    public function setEmotionalEatingScore($value){$this->EmotionalEatingScore = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $LifeStyleScore;
    public function getLifeStyleScore(){return $this->LifeStyleScore;}
    public function setLifeStyleScore($value){$this->LifeStyleScore = $value;}
    /**
    *@var float
    *@Column(type="float", precision=5, scale=2, nullable=false) 
    **/
    protected $EstimatedGoloBMI;
    public function getEstimatedGoloBMI(){return $this->EstimatedGoloBMI;}
    public function setEstimatedGoloBMI($value){$this->EstimatedGoloBMI = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $IdealWeight;
    public function getIdealWeight(){return $this->IdealWeight;}
    public function setIdealWeight($value){$this->IdealWeight = $value;}
    /**
    *@var int
    *@Column(type="string", length=10, nullable=false)
    **/
    protected $IdealWeightRange;
    public function getIdealWeightRange(){return $this->IdealWeightRange;}
    public function setIdealWeightRange($value){$this->IdealWeightRange = $value;}
    /**
    *@var float
    *@Column(type="float", precision=5, scale=2, nullable=false) 
    **/
    protected $IdealBMI;
    public function getIdealBMI(){return $this->IdealBMI;}
    public function setIdealBMI($value){$this->IdealBMI = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $IdealWeightToLose;
    public function getIdealWeightToLose(){return $this->IdealWeightToLose;}
    public function setIdealWeightToLose($value){$this->IdealWeightToLose = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $PercentageToLose;
    public function getPercentageToLose(){return $this->PercentageToLose;}
    public function setPercentageToLose($value){$this->PercentageToLose = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WeightLossGoal;
    public function getWeightLossGoal(){return $this->WeightLossGoal;}
    public function setWeightLossGoal($value){$this->WeightLossGoal = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WeightLossGoalPercentage;
    public function getWeightLossGoalPercentage(){return $this->WeightLossGoalPercentage;}
    public function setWeightLossGoalPercentage($value){$this->WeightLossGoalPercentage = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $RealisticWeightLossGoalPerWeek;
    public function getRealisticWeightLossGoalPerWeek(){return $this->RealisticWeightLossGoalPerWeek;}
    public function setRealisticWeightLossGoalPerWeek($value){$this->RealisticWeightLossGoalPerWeek = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WaistReductionGoal;
    public function getWaistReductionGoal(){return $this->WaistReductionGoal;}
    public function setWaistReductionGoal($value){$this->WaistReductionGoal = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WaistAtWeightLossGoal;
    public function getWaistAtWeightLossGoal(){return $this->WaistAtWeightLossGoal;}
    public function setWaistAtWeightLossGoal($value){$this->WaistAtWeightLossGoal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $PersonalMetabolicRate;
    public function getPersonalMetabolicRate(){return $this->PersonalMetabolicRate;}
    public function setPersonalMetabolicRate($value){$this->PersonalMetabolicRate = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $BreakfastGuide;
    public function getBreakfastGuide(){return $this->BreakfastGuide;}
    public function setBreakfastGuide($value){$this->BreakfastGuide = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $LunchGuide;
    public function getLunchGuide(){return $this->LunchGuide;}
    public function setLunchGuide($value){$this->LunchGuide = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $DinnerGuide;
    public function getDinnerGuide(){return $this->DinnerGuide;}
    public function setDinnerGuide($value){$this->DinnerGuide = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $FitPointsGoalPerWeek;
    public function getFitPointsGoalPerWeek(){return $this->FitPointsGoalPerWeek;}
    public function setFitPointsGoalPerWeek($value){$this->FitPointsGoalPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $FitPointsPerDay;
    public function getFitPointsPerDay(){return $this->FitPointsPerDay;}
    public function setFitPointsPerDay($value){$this->FitPointsPerDay = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $PMRWithFitPoints;
    public function getPMRWithFitPoints(){return $this->PMRWithFitPoints;}
    public function setPMRWithFitPoints($value){$this->PMRWithFitPoints = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}

?>