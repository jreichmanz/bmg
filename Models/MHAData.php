<?php
// /Models/MHAData.php
namespace golo\models;

/**
 * @Entity @Table(name="MHAData")
 **/
class MHAData implements \JsonSerializable {
    
    /**
    *@var int 
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int 
    *@Column(type="integer", nullable=false) 
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var float
    *@Column(type="float", precision=2, scale=1, nullable=false) 
    **/
    protected $Frame;
    public function getFrame(){return $this->Frame;}
    public function setFrame($value){$this->Frame = $value;}
    /**
    *@var datetime
    *@Column(type="datetime", nullable=false)
    **/
    protected $DateCreated;
    public function getDateCreated(){return $this->DateCreated;}
    public function setDateCreated($value){$this->DateCreated = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Weight;
    public function getWeight(){return $this->Weight;}
    public function setWeight($value){$this->Weight = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Waist;
    public function getWaist(){return $this->Waist;}
    public function setWaist($value){$this->Waist = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Activity;
    public function getActivity(){return $this->Activity;}
    public function setActivity($value){$this->Activity = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ExcercisePerWeek;
    public function getExcercisePerWeek(){return $this->ExcercisePerWeek;}
    public function setExcercisePerWeek($value){$this->ExcercisePerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $MealReplacementsPerWeek;
    public function getMealReplacementsPerWeek(){return $this->MealReplacementsPerWeek;}
    public function setMealReplacementsPerWeek($value){$this->MealReplacementsPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ProcessedFoodsPerWeek;
    public function getProcessedFoodsPerWeek(){return $this->ProcessedFoodsPerWeek;}
    public function setProcessedFoodsPerWeek($value){$this->ProcessedFoodsPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $SodasPerWeek;
    public function getSodasPerWeek(){return $this->SodasPerWeek;}
    public function setSodasPerWeek($value){$this->SodasPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $EmotionalEatingRating;
    public function getEmotionalEatingRating(){return $this->EmotionalEatingRating;}
    public function setEmotionalEatingRating($value){$this->EmotionalEatingRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $CravingsRating;
    public function getCravingsRating(){return $this->CravingsRating;}
    public function setCravingsRating($value){$this->CravingsRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $SleepRating;
    public function getSleepRating(){return $this->SleepRating;}
    public function setSleepRating($value){$this->SleepRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $EnergyRating;
    public function getEnergyRating(){return $this->EnergyRating;}
    public function setEnergyRating($value){$this->EnergyRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $MoodRating;
    public function getMoodRating(){return $this->MoodRating;}
    public function setMoodRating($value){$this->MoodRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $StressRating;
    public function getStressRating(){return $this->StressRating;}
    public function setStressRating($value){$this->StressRating = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }

}

?>