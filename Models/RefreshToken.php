<?php
// /Models/RefreshToken.php
namespace golo\models;

/**
* @Entity @Table(name="RefreshToken")
**/
class RefreshToken implements \JsonSerializable {
    

    function __construct($Id){
        $this->Id = $Id;
    }    
    /**
    *@var string
    *@Id
    *@Column(name="Id", type="guid", nullable=false)
    *@GeneratedValue(strategy = "UUID")
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int
    *@Column(name="UserId", type="integer", nullable=false)
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var string
    *@Column(name="AuthClientId", type="guid", nullable=false)
    **/
    protected $AuthClientId;
    public function getAuthClientId(){return $this->AuthClientId;}
    public function setAuthClientId($value){$this->AuthClientId = $value;}
    /**
    *@var datetime
    *@Column(name="IssuedUtc", type="datetime", nullable=true)
    **/
    protected $IssuedUtc;
    public function getIssuedUtc(){return $this->IssuedUtc;}
    public function setIssuedUtc($value){$this->IssuedUtc = $value;}
    /**
    *@var datetime
    *@Column(name="ExpiresUtc", type="datetime", nullable=true)
    **/
    protected $ExpiresUtc;
    public function getExpiresUtc(){return $this->ExpiresUtc;}
    public function setExpiresUtc($value){$this->ExpiresUtc = $value;}
    /**
    *@var string
    *@Column(name="ProtectedTicket", type="text", nullable=true)
    **/
    protected $ProtectedTicket;
    public function getProtectedTicket(){return $this->ProtectedTicket;}
    public function setProtectedTicket($value){$this->ProtectedTicket = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json;
    }
    
}

?>