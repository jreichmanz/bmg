<?php
// /Models/Authentication.php
namespace golo\models;

/**
* @Entity @Table(name="AuthClient")
**/
class AuthClient implements \JsonSerializable {
    
    function __construct($DeviceId){
        $this->DeviceId = $DeviceId;
    }    
    /**
    *@var string
    *@Id
    *@Column(name="Id", type="guid", nullable=false)
    *@GeneratedValue(strategy = "UUID")
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var string
    *@Column(name="DeviceId", type="guid", nullable=false)
    **/
    protected $DeviceId;
    public function getDeviceId(){return $this->DeviceId;}
    public function setDeviceId($value){$this->DeviceId = $value;}
    /**
    *@var string
    *@Column(name="DeviceType", type="text", nullable=true)
    **/
    protected $DeviceType;
    public function getDeviceType(){return $this->DeviceType;}
    public function setDeviceType($value){$this->DeviceType = $value;}
    /**
    *@var string
    *@Column(name="OS", type="text", nullable=true)
    **/
    protected $OS;
    public function getOS(){return $this->OS;}
    public function setOS($value){$this->OS = $value;}
    /**
    *@var string
    *@Column(name="Secret", type="text", nullable=false)
    **/
    protected $Secret;
    public function getSecret(){return $this->Secret;}
    public function setSecret($value){$this->Secret = $value;}
    /**
     *@var bool
     *@Column(name="Active", type="boolean", nullable=false)
    **/
    protected $Active;
    public function getActive(){return $this->Active;}
    public function setActive($value){$this->Active = $value;}
    /**
    *@var int
    *@Column(name="RefreshTokenLifeTime", type="integer", nullable=false)
    **/
    protected $RefreshTokenLifeTime;
    public function getRefreshTokenLifeTime(){return $this->RefreshTokenLifeTime;}
    public function setRefreshTokenLifeTime($value){$this->RefreshTokenLifeTime = $value;}
    /**
    *@var string
    *@Column(name="AllowedOrigin", type="text", nullable=false)
    **/
    protected $AllowedOrigin;
    public function getAllowedOrigin(){return $this->AllowedOrigin;}
    public function setAllowedOrigin($value){$this->AllowedOrigin = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json;
    }
    
}

?>