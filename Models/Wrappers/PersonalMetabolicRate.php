<?php
// /Models/Wrappers/MHAFull.php
namespace golo\models\wrappers;
class PersonalMetabolicRate implements \JsonSerializable {
    protected $PMR;
    public function getPMR(){return $this->PMR;}
    public function setPMR($value){$this->PMR = $value;}
    protected $Date;
    public function getDate(){return $this->Date;}
    public function setDate($value){$this->Date = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>