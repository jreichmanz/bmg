<?php
// /Models/Wrappers/MHAFull.php
namespace golo\models\wrappers;
class MHAFull implements \JsonSerializable {
     /**
    *@var float
    *@Column(type="float", precision=2, scale=1, nullable=false) 
    **/
    protected $Frame;
    public function getFrame(){return $this->Frame;}
    public function setFrame($value){$this->Frame = $value;}
    /**
    *@var datetime
    *@Column(type="datetime", nullable=false)
    **/
    protected $DateCreated;
    public function getDateCreated(){return $this->DateCreated;}
    public function setDateCreated($value){$this->DateCreated = $value;}
    /** 
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Weight;
    public function getWeight(){return $this->Weight;}
    public function setWeight($value){$this->Weight = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Waist;
    public function getWaist(){return $this->Waist;}
    public function setWaist($value){$this->Waist = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Activity;
    public function getActivity(){return $this->Activity;}
    public function setActivity($value){$this->Activity = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ExcercisePerWeek;
    public function getExcercisePerWeek(){return $this->ExcercisePerWeek;}
    public function setExcercisePerWeek($value){$this->ExcercisePerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $MealReplacementsPerWeek;
    public function getMealReplacementsPerWeek(){return $this->MealReplacementsPerWeek;}
    public function setMealReplacementsPerWeek($value){$this->MealReplacementsPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ProcessedFoodsPerWeek;
    public function getProcessedFoodsPerWeek(){return $this->ProcessedFoodsPerWeek;}
    public function setProcessedFoodsPerWeek($value){$this->ProcessedFoodsPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $SodasPerWeek;
    public function getSodasPerWeek(){return $this->SodasPerWeek;}
    public function setSodasPerWeek($value){$this->SodasPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $EmotionalEatingRating;
    public function getEmotionalEatingRating(){return $this->EmotionalEatingRating;}
    public function setEmotionalEatingRating($value){$this->EmotionalEatingRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $CravingsRating;
    public function getCravingsRating(){return $this->CravingsRating;}
    public function setCravingsRating($value){$this->CravingsRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $SleepRating;
    public function getSleepRating(){return $this->SleepRating;}
    public function setSleepRating($value){$this->SleepRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $EnergyRating;
    public function getEnergyRating(){return $this->EnergyRating;}
    public function setEnergyRating($value){$this->EnergyRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $MoodRating;
    public function getMoodRating(){return $this->MoodRating;}
    public function setMoodRating($value){$this->MoodRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $StressRating;
    public function getStressRating(){return $this->StressRating;}
    public function setStressRating($value){$this->StressRating = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $ProcessedFoodScore;
    public function getProcessedFoodScore(){return $this->ProcessedFoodScore;}
    public function setProcessedFoodScore($value){$this->ProcessedFoodScore = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $EmotionalEatingScore;
    public function getEmotionalEatingScore(){return $this->EmotionalEatingScore;}
    public function setEmotionalEatingScore($value){$this->EmotionalEatingScore = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $LifeStyleScore;
    public function getLifeStyleScore(){return $this->LifeStyleScore;}
    public function setLifeStyleScore($value){$this->LifeStyleScore = $value;}
    /**
    *@var float
    *@Column(type="float", precision=5, scale=2, nullable=false) 
    **/
    protected $EstimatedGoloBMI;
    public function getEstimatedGoloBMI(){return $this->EstimatedGoloBMI;}
    public function setEstimatedGoloBMI($value){$this->EstimatedGoloBMI = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $IdealWeight;
    public function getIdealWeight(){return $this->IdealWeight;}
    public function setIdealWeight($value){$this->IdealWeight = $value;}
    /**
    *@var int
    *@Column(type="string", length=10, nullable=false)
    **/
    protected $IdealWeightRange;
    public function getIdealWeightRange(){return $this->IdealWeightRange;}
    public function setIdealWeightRange($value){$this->IdealWeightRange = $value;}
    /**
    *@var float
    *@Column(type="float", precision=5, scale=2, nullable=false) 
    **/
    protected $IdealBMI;
    public function getIdealBMI(){return $this->IdealBMI;}
    public function setIdealBMI($value){$this->IdealBMI = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $IdealWeightToLose;
    public function getIdealWeightToLose(){return $this->IdealWeightToLose;}
    public function setIdealWeightToLose($value){$this->IdealWeightToLose = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $PercentageToLose;
    public function getPercentageToLose(){return $this->PercentageToLose;}
    public function setPercentageToLose($value){$this->PercentageToLose = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WeightLossGoal;
    public function getWeightLossGoal(){return $this->WeightLossGoal;}
    public function setWeightLossGoal($value){$this->WeightLossGoal = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WeightLossGoalPercentage;
    public function getWeightLossGoalPercentage(){return $this->WeightLossGoalPercentage;}
    public function setWeightLossGoalPercentage($value){$this->WeightLossGoalPercentage = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $RealisticWeightLossGoalPerWeek;
    public function getRealisticWeightLossGoalPerWeek(){return $this->RealisticWeightLossGoalPerWeek;}
    public function setRealisticWeightLossGoalPerWeek($value){$this->RealisticWeightLossGoalPerWeek = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WaistReductionGoal;
    public function getWaistReductionGoal(){return $this->WaistReductionGoal;}
    public function setWaistReductionGoal($value){$this->WaistReductionGoal = $value;}
    /**
    *@var float
    *@Column(type="float", precision=8, scale=4, nullable=false) 
    **/
    protected $WaistAtWeightLossGoal;
    public function getWaistAtWeightLossGoal(){return $this->WaistAtWeightLossGoal;}
    public function setWaistAtWeightLossGoal($value){$this->WaistAtWeightLossGoal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $PersonalMetabolicRate;
    public function getPersonalMetabolicRate(){return $this->PersonalMetabolicRate;}
    public function setPersonalMetabolicRate($value){$this->PersonalMetabolicRate = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $BreakfastGuide;
    public function getBreakfastGuide(){return $this->BreakfastGuide;}
    public function setBreakfastGuide($value){$this->BreakfastGuide = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $LunchGuide;
    public function getLunchGuide(){return $this->LunchGuide;}
    public function setLunchGuide($value){$this->LunchGuide = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $DinnerGuide;
    public function getDinnerGuide(){return $this->DinnerGuide;}
    public function setDinnerGuide($value){$this->DinnerGuide = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $FitPointsGoalPerWeek;
    public function getFitPointsGoalPerWeek(){return $this->FitPointsGoalPerWeek;}
    public function setFitPointsGoalPerWeek($value){$this->FitPointsGoalPerWeek = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $FitPointsPerDay;
    public function getFitPointsPerDay(){return $this->FitPointsPerDay;}
    public function setFitPointsPerDay($value){$this->FitPointsPerDay = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $PMRWithFitPoints;
    public function getPMRWithFitPoints(){return $this->PMRWithFitPoints;}
    public function setPMRWithFitPoints($value){$this->PMRWithFitPoints = $value;}

    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>