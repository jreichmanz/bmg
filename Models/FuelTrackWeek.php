<?php
// /Models/FuelTrackWeek.php
namespace golo\models;
/**
 * @Entity @Table(name="FuelTrackWeek")
 **/
class FuelTrackWeek implements \JsonSerializable {
    /**
    *@var int 
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int 
    *@Column(type="integer", nullable=false) 
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Week;
    public function getWeek(){return $this->Week;}
    public function setWeek($value){$this->Week = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $Year;
    public function getYear(){return $this->Year;}
    public function setYear($value){$this->Year = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $FitPointsGoal;
    public function getFitPointsGoal(){return $this->FitPointsGoal;}
    public function setFitPointsGoal($value){$this->FitPointsGoal = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=true) 
    **/
    protected $MetabolicRate;
    public function getMetabolicRate(){return $this->MetabolicRate;}
    public function setMetabolicRate($value){$this->MetabolicRate = $value;}
    public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>