<?php
// /Models/UserDetails.php
namespace golo\models;
/**
 * @Entity @Table(name="UserDetails")
 **/
class UserDetails implements \JsonSerializable {
    /**
    *@var integer 
    *@Id 
    *@Column(name="Id", type="integer", nullable=false) 
    *@GeneratedValue(strategy = "AUTO") 
    **/
    protected $Id;
    public function getId(){return $this->Id;}
    public function setId($value){$this->Id = $value;}
    /**
    *@var int
    *@Column(type="integer", nullable=false)
    **/
    protected $UserId;
    public function getUserId(){return $this->UserId;}
    public function setUserId($value){$this->UserId = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $Sex;
    public function getSex(){return $this->Sex;}
    public function setSex($value){$this->Sex = $value;}
    /**
    *@var int
    *@Column(type="integer", nullable=false)
    **/
    protected $Year;
    public function getYear(){return $this->Year;}
    public function setYear($value){$this->Year = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $HeightFT;
    public function getHeightFT(){return $this->HeightFT;}
    public function setHeightFT($value){$this->HeightFT = $value;}
    /**
    *@var int
    *@Column(type="smallint", nullable=false)
    **/
    protected $HeightIN;
    public function getHeightIN(){return $this->HeightIN;}
    public function setHeightIN($value){$this->HeightIN = $value;}
   public function jsonSerialize() {
        $json = array();
        foreach($this as $key => $value) {
            if($value instanceof \DateTime){
                $json[$key] = $value->format('m-d-Y');
            } else {
                $json[$key] = $value;
            }
            
        }
        return $json; 
    }
}
?>