<?php

namespace golo;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require "api/Utilities/TokenUtility.php";
require "Utilities/RegisterUtilities.php";
require "Models/RegisterModels.php";
require "Bearer.php";
require "AccessToken.php";
require "encryption.php";
require "debug.php";
$log = true;
$fatalLog = true;
class GoloContext {
    private $conn;
    private $isDevMode;
    private $config;
    private $db;
    function __construct(){
        
        $this->isDevMode = true;
        
        $this->config = Setup::createAnnotationMetadataConfiguration(array(realpath("./Models/")), $this->isDevMode);
        $this->conn = array(
        'dbname' => 'goloportal',
        'user' => 'goloportal',
        'password' => 'testtest',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
        );
        $this->db = EntityManager::create($this->conn, $this->config);
    }
    function db(){
        return $this->db;
    }
}


?>